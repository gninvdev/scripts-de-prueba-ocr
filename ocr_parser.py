"""
    ocr_parser.py

    Módulo de OCR y manipulación de diccionarios de coordenadas.

"""

import time

# region Tesseract


def get_tesseract(image_file: str):
    from PIL import Image
    import pytesseract
    from pytesseract.pytesseract import Output
    """
        Devuelve el texto usando tesseract en forma de diccionario

        ### Parameters
        image_file : str
            - Ubicacion del archivo

        ### Returns
        ocr_text : dict
            - Texto de la imagen con coordenadas
    """
    
    with Image.open(image_file) as img:
        return pytesseract.image_to_data(img, lang="spa", output_type=Output.DICT)


def full_text_tesseract(image_file: str):
    import cv2
    import pytesseract
    """
        Devuelve el texto completo usando tesseract

        ### Parameters
        image_file : str
            - Ubicacion del archivo

        ### Returns
        ocr_text : str
            - Texto de la imagen
    """
    img = cv2.imread(image_file)

    ocr_text = pytesseract.image_to_string(img, lang="spa")
    return ocr_text


def tesseract_to_line_data(tesseract_data) -> dict:
    """
        Devuelve un diccionario con coordenadas y texto basado en el OCR
        de tesseract

        ### Parameters
        tesseract_data : dict
            Diccionario con los datos de tesseract

        ### Returns
        data : dict
            Diccionario con las coordenadas y texto
            - Keys:
                - coord : list[tuple[int, int, int, int]]
                    - Lista de coordenadas
                - text : list[str]
                    - Lista de texto
    """

    data: dict = {}
    data['text'] = [""]
    data['coord'] = [(0, 0, 0, 0)]

    original = tesseract_data

    n_boxes: int = len(original['level'])

    line_n: int = 0

    for i in range(n_boxes):
        if original['level'][i] == 4:  # level 4 = línea
            pos = (original['left'][i], original['top'][i], original['width'][i] +
                   original['left'][i], original['height'][i] + original['top'][i])
            data['coord'].append(pos)
            line_n += 1
        elif original['level'][i] == 5:  # level 5 = palabra
            if len(data['text']) <= line_n:
                data['text'].append(original['text'][i])
            else:
                data['text'][line_n] += " " + original['text'][i]

    return data
#endregion

# region EasyOCR

def get_easyocr(image_file: str, use_gpu: bool = False):
    import easyocr
    """
        Devuelve el texto usando EasyOCR

        ### Parameters
        image_file : str
            - Ubicacion del archivo

        ### Returns
        ocr_text
            - Texto de la imagen con coordenadas
    """

    reader = easyocr.Reader(['es'], gpu=use_gpu)
    # TODO: Probar con estos valores para conseguir mejor las líneas
    return reader.readtext(image_file, decoder="greedy")


def full_text_easyocr(image_file: str, use_gpu: bool = False):
    import easyocr
    """
        Devuelve el texto completo usando EasyOCR

        ### Parameters
        image_file : str
            - Ubicacion del archivo
        use_gpu : bool, (default False)
            - Usa la GPU si es True

        ### Returns
        ocr_text : str
            - Texto de la imagen
    """

    reader = easyocr.Reader(['es'], gpu=use_gpu)
    result = reader.readtext(image_file, decoder="wordbeamsearch", batch_size=3, detail=0, paragraph=True)

    return "\n".join(result)


def easyocr_to_line_data(easy_result):
    """
        Hace OCR con EasyOCR y devuelve un diccionario con coordenadas y texto

        ### Parameters
        easy_result

        ### Returns
        data : dict
            Diccionario con las coordenadas y texto
            - Keys:
                - coord : list[tuple[int, int, int, int]]
                    - Lista de coordenadas
                - text : list[str]
                    - Lista de texto
    """

    data: dict = {}
    data['text'] = []
    data['coord'] = []

    result = easy_result

    n_boxes = len(result)

    for i in range(n_boxes):

        pos = (result[i][0][0][0], result[i][0][0][1],
               result[i][0][2][0], result[i][0][2][1])
        data['coord'].append(pos)
        data['text'].append(result[i][1])

    return data
#endregion

#region ABBYY

def abbyy_file_to_line_data(original_file: str) -> dict:
    """
        Parsea el archivo de OCR de ABBYY

        ### Parameters
        original_file : str
            Ubicación del archivo

        ### Returns
        data : dict
            Diccionario con las coordenadas y texto
            - Keys:
                - coord : list[tuple[int, int, int, int]]
                    - Lista de coordenadas
                - text : list[str]
                    - Lista de texto
    """

    original: list
    data: dict = {}
    data['text'] = []
    data['coord'] = []

    with open(original_file, "r+", encoding="utf-8") as fp:
        original = fp.readlines()

    for line in original:
        if line[:3] == "RE>":  # Texto
            data['text'].append(line[3:])
        if line[:3] == "IS>":  # Coordenadas
            numbers = line[3:].split()
            data['coord'].append((int(numbers[0]), int(
                numbers[1]), int(numbers[2]), int(numbers[3])))

    return data
#endregion

#region Google

def google_to_document(image_file: str):
    from google.cloud import vision
    """
        Hace OCR con Google Cloud Vision y devuelve un diccionario con coordenadas y texto
        NOTA: recordar asignar la API key como variable de entorno

        ### Parameters
        image_file : str
            Ubicación del archivo

        ### Returns
        data : dict
            Diccionario con las coordenadas y texto
    """
    client = vision.ImageAnnotatorClient()

    with open(image_file, 'rb') as imf:
        content = imf.read()

    image = vision.Image(content=content)

    response = client.document_text_detection(image=image)

    if response.error.message:
        raise Exception(
            '{}\nFor more info on error messages, check: '
            'https://cloud.google.com/apis/design/errors'.format(
                response.error.message))

    document = response.full_text_annotation

    return document
#endregion

#region Azure

def get_azure(image_url: str, key: str = ""):
    from azure.cognitiveservices.vision.computervision import ComputerVisionClient
    from msrest.authentication import CognitiveServicesCredentials
    """
        Devuelve el texto usando Azure en base a una URL

        ### Parameters
        image_url : str
            - URL del archivo

        ### Returns
        ocr_text
            - Texto de la imagen con coordenadas
    """

    endpoint = "https://ryd-ocr.cognitiveservices.azure.com/"
    computervision_client = ComputerVisionClient(
        endpoint, CognitiveServicesCredentials(key))
    read_response = computervision_client.read(image_url,  raw=True)

    read_operation_location = read_response.headers["Operation-Location"]
    operation_id = read_operation_location.split("/")[-1]

    while True:
        read_result = computervision_client.get_read_result(operation_id)
        if read_result.status not in ['notStarted', 'running']:
            break
        time.sleep(1)

    return read_result


def get_azure_stream(image_stream, key: str = ""):
    from azure.cognitiveservices.vision.computervision import ComputerVisionClient
    from msrest.authentication import CognitiveServicesCredentials
    """
        Devuelve el texto usando Azure en base a un stream de imagen

        ### Parameters
        image_stream
            - Stream de la imagen

        ### Returns
        ocr_text
            - Texto de la imagen con coordenadas
    """

    endpoint = "https://ryd-ocr.cognitiveservices.azure.com/"
    computervision_client = ComputerVisionClient(
        endpoint, CognitiveServicesCredentials(key))
    read_response = computervision_client.read_in_stream(
        image_stream,  raw=True)

    read_operation_location = read_response.headers["Operation-Location"]
    operation_id = read_operation_location.split("/")[-1]

    while True:
        read_result = computervision_client.get_read_result(operation_id)
        if read_result.status not in ['notStarted', 'running']:
            break
        time.sleep(1)

    return read_result


def azure_to_line_data(read_result):
    from azure.cognitiveservices.vision.computervision.models import OperationStatusCodes
    """
        Hace OCR con Azure y devuelve un diccionario con coordenadas y texto

        ### Parameters
        image_url : str
            URL del archivo
        key : str
            Clave de suscripción

        ### Returns
        data : dict
            Diccionario con las coordenadas y texto
            - Keys:
                - coord : list[tuple[float, float, float, float]]
                    - Lista de coordenadas
                - text : list[str]
                    - Lista de texto
    """

    data: dict = {}
    data['text'] = []
    data['coord'] = []

    if read_result.status == OperationStatusCodes.succeeded:
        for text_result in read_result.analyze_result.read_results:
            for line in text_result.lines:
                for word in line.words:
                    data['text'].append(word.text)
                    pos = (word.bounding_box[0], word.bounding_box[1],
                           word.bounding_box[4], word.bounding_box[5])
                    data['coord'].append(pos)

    return data
#endregion

#region Diccionario de líneas

def data_to_string(coord_data: dict) -> str:
    """
        Devuelve un string en base al diccionario de coordenadas por lineas

        ### Parameters
        coord_data : dict
            Diccionario de coordenadas
            - Keys:
                - coord : list[tuple[int, int, int, int]]
                    - Lista de coordenadas
                - text : list[str]
                    - Lista de texto

        ### Returns
        result : str
            String con cada línea de texto y sus coordenadas
    """

    result: str = ""
    n: int = len(coord_data['coord'])

    for i in range(n):
        result += str(coord_data['coord'][i]) + " " + \
            coord_data['text'][i].rstrip() + "\n"

    return result
#endregion

#region Archivo corregido

def corrected_file_to_line_data(coord_file: str, text_file: str) -> dict:
    """
        Parsea el archivo de OCR de ABBYY con el texto original

        ### Parameters
        coord_file : str
            Ubicación del archivo ocr de ABBYY con las coordenadas
        text_file : str
            Ubicación del archivo de texto original

        ### Returns
        data : dict
            Diccionario con las coordenadas y texto
            - Keys:
                - coord : list[tuple[int, int, int, int]]
                    - Lista de coordenadas
                - text : list[str]
                    - Lista de texto
    """

    original: list
    data: dict = {}
    data['text'] = []
    data['coord'] = []

    with open(coord_file, "r+", encoding="utf-8") as fp:
        original = fp.readlines()

    with open(text_file, "r+", encoding="utf-8") as fp:
        original_text = fp.readlines()

    i = 0
    for line in original:
        if line[:3] == "IS>":  # Coordenadas
            if i < len(original_text):
                numbers = line[3:].split()
                data['coord'].append((int(numbers[0]), int(
                    numbers[1]), int(numbers[2]), int(numbers[3])))
                data['text'].append(original_text[i])
                i += 1

    return data
#endregion