**Scripts varios para comparar el rendimiento entre motores de OCR.**

---

## compare_full_text.py

Hace OCR a una imagen y lo compara con el texto original.

Devuelve por consola el tiempo que tarda el OCR y la precisión.
Opcionalmente devuelve un html con la comparación entre los textos.

*Uso*:

compare_full_text.py [-h] [--html] (-t | -e) imagen_entrada texto_entrada

*Argumentos*:

**imagen_entrada**  ubicación de la imagen

**texto_entrada**   ubicación del texto original

**-t, --tesseract** utiliza tesseract como motor

**-e, --easyocr**   utiliza EasyOCR como motor

**-eg, --easyocrgpu** utiliza EasyOCR como motor (con GPU)

*Opcionales*:

**-h, --help**      mensaje de ayuda

**--html**          crea un html con las diferencias

---

## compare_lines.py

Hace OCR a una imagen y lo compara con el texto original.
A diferencia de comparacion_ocr.txt, este compara por coordenadas.

Devuelve por consola la precisión del OCR.
Opcionalmente devuelve un txt con la comparación entre los textos.

*Uso*:

compare_lines.py [-h] [--txt] (-t | -a | -g | -e | -eg) imagen_entrada ocr_entrada texto_entrada

*Argumentos*:

**imagen_entrada**  ubicación de la imagen

**ocr_entrada**   ubicación del texto de ABBYY con coordenadas (no acepta el .OCR directo, hay que pasarlo a otra codificación antes)

**texto_entrada**   ubicación del texto original

**-t, --tesseract** utiliza tesseract como motor

**-a, --abbyy**     utiliza el archivo de OCR de ABBYY para comparar

**-g, --google**    utiliza Google Cloud Vision como motor (NOTA: setear la API Key como variable de entorno)

**-e, --easyocr**   utiliza EasyOCR como motor (con CPU)

**-eg, --easyocrgpu**   utiliza EasyOCR como motor (con GPU)

**-az, --azure**    utiliza Azure como motor, pasar clave de suscripción como argumento

*Opcionales*:

**-h, --help**      mensaje de ayuda

**--txt**           crea un txt con las diferencias

*Formato del txt*:

> **OVERALL PRECISION**: Precisión promedio

> **CO>**: Coordenadas (Original -> Match OCR)

> **OG>**: Texto del original

> **OC>**: Texto del OCR

> **RA>**: Ratio de precisión

---

## image_hsv_playground.py

Permite editar los parámetros de color en sistema HSV de una imagen para realizar un filtro por colores.
Indicar el path absoluto de la imagen o poner el archivo '.py' y la imagen en la misma carpeta.

Devuelve por consola los parámetros seleccionados (por si fueran necesarios para algún filtro).

*Uso*:

image_hsv_playground.py [-h] [-m] imagen_entrada

*Argumentos*:

**imagen_entrada**  ubicación de la imagen.

*Opcionales*:

**-h, --help**      mensaje de ayuda

**-m, --mask**      muestra la mascara

---