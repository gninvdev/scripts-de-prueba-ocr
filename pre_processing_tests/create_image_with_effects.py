import cv2
import os
import image_auxiliar_funcs as aux
import effect_model
import preprocessings_combinations_summary as prepro

# Create some tests to see performance!

usable_files = [3, 4, 5, 7]


# --------------------------------------------------------------------------
# TEST 1: Create the image the combination matrix said was the best and compare OCRs.

# 1. Load original images:
main_directory = "/home/sgarcia/Desktop/Global News/Proyects/OCR_ROC/scripts-de-prueba-ocr/ejemplos/Test"
imageDir = []
original_images = []
for file_index in usable_files:
    imageDir.append(main_directory + f"/{file_index}_IMG.jpeg")
    original_image, _ = aux.load_image_file(imageDir[-1])
    original_images.append(original_image)

images_with_effects = original_images.copy()
# 2. Apply effects (for best OCR) to each image:
for effect in ['Grayscale', 'Gaussian Blurr-9', 'Otsu Threshold', 'Open-3']:
    size = int(effect.split('-')[-1]) if len(effect.split('-')) > 1 else None
    images_with_effects[0], _ = aux.aplly_image_effect(images_with_effects[0], effect=effect, size=size)
for effect in ['Grayscale', 'Gaussian Blurr-5', 'Otsu Threshold']:
    size = int(effect.split('-')[-1]) if len(effect.split('-')) > 1 else None
    images_with_effects[1], _ = aux.aplly_image_effect(images_with_effects[1], effect=effect, size=size)
for effect in ['Grayscale']:
    size = int(effect.split('-')[-1]) if len(effect.split('-')) > 1 else None
    images_with_effects[2], _ = aux.aplly_image_effect(images_with_effects[2], effect=effect, size=size)
for effect in []:
    size = int(effect.split('-')[-1]) if len(effect.split('-')) > 1 else None
    images_with_effects[3], _ = aux.aplly_image_effect(images_with_effects[3], effect=effect, size=size)

# 3. Save images:
new_images_path = "/home/sgarcia/Desktop/Global News/Proyects/OCR_ROC/scripts-de-prueba-ocr/pre_processing_tests/image_with_effects"
os.chdir(new_images_path)

for index, file_index in enumerate(usable_files):
    cv2.imwrite(f"image_{file_index}_with_effects.jpeg", images_with_effects[index])


# --------------------------------------------------------------------------
# TEST 2: Create a file with a fix set of effects and compare OCRs.
fix_effect_list = [effect_model.Effect('Grayscale'), effect_model.Effect('Gaussian Blurr', 3), effect_model.Effect('Otsu Threshold'), effect_model.Effect('Open', 3), effect_model.Effect('Canny')]
fix_effects = effect_model.EffectsMatrix()
fix_effects.matrix = [fix_effect_list]
log_file = open("fix-effects_pre-processing log.txt", 'w+')

images_with_effects = original_images.copy()
for index, file_index in enumerate(usable_files):
    log_file.write(f"Start with file: {file_index}.\n")
    prepro.get_preproseccing_summary(effect_matrix=fix_effects, main_directory=main_directory, file_index=file_index, log_file=log_file, top_n=1, ocr_engine='Tesseract', print_results=False)
    
    for effect in ['Grayscale', 'Gaussian Blurr-3', 'Otsu Threshold', 'Open-3', 'Canny']:
        size = int(effect.split('-')[-1]) if len(effect.split('-')) > 1 else None
        images_with_effects[index], _ = aux.aplly_image_effect(images_with_effects[index], effect=effect, size=size)
        # cv2.imshow(f"Image {index} with {effect}.", images_with_effects[index])
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

    cv2.imwrite(f"image_{file_index}_with_fix_effects.jpeg", images_with_effects[index])

log_file.close()
