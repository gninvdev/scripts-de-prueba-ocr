# ------------- GET ALL POSIBLE COMBINATIONS ------------------- 
# from .effect_model import *
from effect_model import *

# 1. Grayscaling (on/off). -> Escala de grises.
# 2. Blurr: ['Median Blurr', 'Gaussian Blurr'] & size.
# 3.1. OPCIONAL: Cambio de espacio de colores.
# 3.2. Threshold: ['Adaptative Threshold', 'Otsu Threshold'] -> Black & White (bits).
# 4. Open & size -> (Aca podria hacer un bitwise AND y tener una imagen a color).
# 5. Canny & size.

def get_all_combinations_effect_matrix():

    # 0. Creo la Matriz de Efectos:
    effect_matrix = EffectsMatrix()

    # 1. Grayscaling --> Escala de grises.
    grayscaling = Effect('Grayscale', None)
    effect_matrix.addEffect(grayscaling)

    # 2. Blurr.
    blurring = EffectFamily('Blurr', variants=['Median', 'Gaussian'], sizes=[3, 5, 7, 9])
    effect_matrix.addEffectFamily(blurring)

    # 3.1. OPCIONAL: Cambio de espacio de colores.
    # 3.2. Threshold: ['Adaptative Threshold', 'Otsu Threshold'] -> Black & White (bits).
    thresholding = EffectFamily('Threshold', variants=['Otsu'], sizes=None)
    effect_matrix.addEffectFamily(thresholding, predecessor=grayscaling)

    # 4. Open & size -> (Aca podria hacer un bitwise AND y tener una imagen a color).
    opening = EffectFamily('Open', variants=None, sizes=[1, 3, 5, 7, 9])
    effect_matrix.addEffectFamily(opening)

    # 5. Canny & size.
    canning = EffectFamily('Canny', variants=None, sizes=None)
    effect_matrix.addEffectFamily(canning)

    return effect_matrix


# ------------- APPLY EFFECTS AND GET THE 'N' BEST ONES ------------
# import imageFuncs
# import pre_processing_tests
import image_auxiliar_funcs as aux # from . import image_auxiliar_funcs as aux
import cv2
import numpy as np
from PIL import Image
from datetime import datetime
import math

def get_preproseccing_summary(effect_matrix:EffectsMatrix, main_directory:str, file_index:int, log_file, top_n:int, ocr_engine:str, print_results:bool):

    # Get starting time:
    start_time = datetime.now()
    log_file.write(f"\nFile {file_index} process starts at: {start_time}.\n\n")

    # Load original image:
    imageDir = main_directory + f"/{file_index}_IMG.jpeg"
    image, _ = aux.load_image_file(imageDir)

    # Get OCR without Pre-Processing:
    textCordDir = main_directory + f"/{file_index}_COORD.txt"
    textDir = main_directory + f"/{file_index}_TEXT.txt"
    ocr_ratio = aux.text_comparator(image, textCordDir, textDir, ocr_engine)
    print(f"Original image has a ratio of: {ocr_ratio} %.")
    log_file.write(f"--> {ocr_engine}:\n")
    log_file.write(f"Original image has a ratio of: {ocr_ratio} %.\n")

    ranking = [{'effect list':[], 'ratio':0.0} for _ in range(top_n)] # [{'effects_list': List[Effect], 'precition': float}]
    min_ranking_val = 0.0

    # Apply each list of effects to image and compare texts
    for row_index, effect_list in enumerate(effect_matrix.matrix ):

        # Make copy of the original image.
        image_copy = image.copy()
        
        # Apply all efects
        all_ok = True
        for effect in effect_list:
            image_copy, all_ok = aux.aplly_image_effect(image_copy, effect.name, effect.size)
            if not all_ok: break
        
        if all_ok:
            # Get OCR ratio:
            ocr_ratio = aux.text_comparator(image_copy, textCordDir, textDir, ocr_engine)
            ocr_ratio = round(ocr_ratio, 5)
            
            # Print Information:
            if print_results:
                print(f"{row_index}. Ratio: {ocr_ratio}%. - Applyed effects: {len(effect_list)} --> {effect_list}.")
                log_file.write(f"{row_index}.\t\tRatio: {ocr_ratio} %.\tApplyed effects: {len(effect_list)} --> {effect_list}.\n")
            
            # Compare texts:
            if ocr_ratio > min_ranking_val: #useless_threshold:
                for index, element in enumerate(ranking):
                    if ocr_ratio > element['ratio']:
                        ranking.insert(index, {'effect list':effect_list, 'ratio':ocr_ratio})
                        ranking = ranking[0:top_n]
                        min_ranking_val = ranking[top_n-1]['ratio']
                        # ranking[index]['effect list'] = effect_list
                        # ranking[index]['ratio'] = ocr_ratio
                        break
        else:
            # Print Information:
            if print_results:
                print(f"{row_index}. ERROR applying effect of list --> {effect_list}.")
                log_file.write(f"{row_index}.\t\tERROR.\tEffects list: {len(effect_list)} --> {effect_list}.\n")
    
    # Print Results:
    log_file.write(f"\n\nThe best {top_n} are:\n")
    for index, element in enumerate(ranking):
        log_file.write(f"{index+1}.\t{element['ratio']} % --> {element['effect list']}.\n")

    # Get Process time:
    end_time = datetime.now()
    log_file.write(f"\nProcess ends at: {end_time}.\n\n")

    delta_in_secs = math.floor((end_time - start_time).total_seconds())
    delta_in_mins = math.floor(delta_in_secs/60)
    delta_in_hs = delta_in_mins/60
    
    if delta_in_hs < 1:
        if delta_in_mins < 1:
            log_file.write(f"Process takes less than one minute.\n")
        else:
            log_file.write(f"Process takes {math.floor(delta_in_mins)} minutes.\n")
    else:
        horas = math.floor(delta_in_hs)
        minutos = delta_in_mins - horas*60
        log_file.write(f"Process takes {horas} hour/s and {minutos} minute/s.\n")
        
    print("\n" + '-'*30 + "\n\n")
    log_file.write("\n" + '-'*30 + "\n\n")
