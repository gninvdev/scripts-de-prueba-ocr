# Contains all GUI functions.
import os, sys; sys.path.append(os.path.dirname(os.path.realpath(__file__)))
import io, re
import cv2
import numpy as np

from PIL import Image
from pytesseract.pytesseract import Output
from pytesseract import *
import pytesseract as pytess
import requests
# import easyocr

import ocr_parser as Parser
import full_text_comparator as Comparator


# IMAGE FUNCTIONS ----------------------

def load_image_file(image_filename_path):
    try:
        if os.path.exists(image_filename_path):    # Cargo la imagen como PIL
            cv2_image, pil_image = [], []
            pil_image = Image.open(image_filename_path)
            cv2_image = cv2.cvtColor(np.array(pil_image), cv2.COLOR_BGR2RGB)
            return cv2_image, pil_image
        else:
            raise Exception("Unknown path or directory.")
    except:
        sys.exit('El archivo o directorio no existe!')


# IMAGE PROCESSES ----------------------

def aplly_image_effect(image, effect:str, size=None):
    all_ok = True
    try:
        if re.search('Grayscale', effect):
            # print("Applying: Grayscaling.")
            image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        elif re.search('Median Blurr', effect):
            # print(f"Applying: Median Blurring with size {size}.")
            image = cv2.medianBlur(image, size)
        elif re.search('Gaussian Blurr', effect):
            # print("Applying: Gaussian Blurring with size {size}.")
            kernel = (size,size)
            image = cv2.GaussianBlur(image, kernel, 0)
        elif re.search('Adaptive Threshold', effect):
            # print("Applying: Adaptive Thresholding with size {size}.")
            image = cv2.adaptiveThreshold(image, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, size, 0)
        elif re.search('Otsu Threshold', effect):
            # print("Applying: Otsu Thresholding with size {size}.")
            image = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
        elif re.search('Open', effect):
            # print("Applying: Opening with size {size}.")
            kernel = np.ones((size,size),np.uint8)
            image = cv2.morphologyEx(image, cv2.MORPH_OPEN, kernel)
        elif re.search('Canny', effect):
            # print("Applying: Cannying")
            image = cv2.Canny(image, 100, 200)
        else:
            print(f"Unknown effect: '{effect}'.")
            all_ok = False
    except:
        # sys.exit('Error applying unknown effect.')
        print(f"Error applying effect: '{effect}'.")
        all_ok = False
    return image, all_ok


# OCR & TEXT FUNCTIONS ----------------------

def text_comparator(image, coords_text_file_path:str, original_text_file_path:str, engine="Tesseract") -> float:

    original_data = Parser.corrected_file_to_line_data(coords_text_file_path, original_text_file_path)

    if engine == "Tesseract":
        t_data = pytess.image_to_data(image, lang="spa", output_type=Output.DICT)
        ocr_data = Parser.tesseract_to_line_data(t_data)
        return Comparator.compare_line_data(original_data, ocr_data, "tesseract", to_file=False)
    elif engine == "Azure":
        new_images_path = "/home/sgarcia/Desktop/Global News/Proyects/OCR_ROC/scripts-de-prueba-ocr/pre_processing_tests/image_with_effects"
        os.chdir(new_images_path)
        cv2.imwrite("temporary_image.jpeg", image)
        image = open("temporary_image.jpeg", "rb")

        azure_key = os.environ.get("AZURE_SECRET_KEY")
        a_data = Parser.get_azure_stream(image, azure_key)
        ocr_data = Parser.azure_to_line_data(a_data)
        return Comparator.compare_line_data(original_data, ocr_data, "azure", to_file=False)

    else:
        print("Unknown OCR engine.")
        return 0.0



# TESTING -------------------------------------------------------

# FUNCTION 1: load_image_file(image_filename_path)
def function1Test(apply:bool) -> None:
    if apply:
        # Test 1: Invalid Path:
        image_file_path = "/home/sgarcia/Desktop/Global News/Proyects/OCR_ROC/scripts-de-prueba-ocr/ejemplos/Test/IMG.jpeg"
        # cv2_image, _ = load_image_file(image_file_path)

        # Test 2: Valid Path:
        image_file_path = "/home/sgarcia/Desktop/Global News/Proyects/OCR_ROC/scripts-de-prueba-ocr/ejemplos/Test/7_IMG.jpeg"
        cv2_image, _ = load_image_file(image_file_path)
        cv2.imshow("FUNCTION 1: Test 2: load_image_file", cv2_image); cv2.waitKey(0); cv2.destroyAllWindows()


# FUNCTION 2: aplly_image_effect(image, effect:str, size)
def function2Test(apply:bool) -> None:
    if apply:
        image_file_path = "/home/sgarcia/Desktop/Global News/Proyects/OCR_ROC/scripts-de-prueba-ocr/ejemplos/Test/7_IMG.jpeg"
        cv2_image, _ = load_image_file(image_file_path)
        
        # Test 1: Invalid effect
        image, _ = aplly_image_effect(cv2_image.copy(), "Invalid", size=None)

        # Test 2: Grayscale
        image, all_ok = aplly_image_effect(cv2_image.copy(), "Grayscale", size=None)
        cv2.imshow("FUNCTION 2: Test 2: aplly_image_effect - Grayscale", image); cv2.waitKey(0); cv2.destroyAllWindows()

        # Test 3: Median Blurr
        image, all_ok = aplly_image_effect(cv2_image.copy(), "Median Blurr-3", size=7)
        cv2.imshow("FUNCTION 2: Test 3: aplly_image_effect - Median Blurr", image); cv2.waitKey(0); cv2.destroyAllWindows()

        # Test 4: Gaussian Blurr
        image, all_ok = aplly_image_effect(cv2_image.copy(), "Gaussian Blurr-3", size=7)
        cv2.imshow("FUNCTION 2: Test 4: aplly_image_effect - Gaussian Blurr", image); cv2.waitKey(0); cv2.destroyAllWindows()

        # Test 5: Otsu Threshold
        image, all_ok = aplly_image_effect(cv2_image.copy(), "Grayscale", size=None)
        image, all_ok = aplly_image_effect(image, "Otsu Threshold", size=None)
        cv2.imshow("FUNCTION 2: Test 5: aplly_image_effect - Otsu Threshold", image); cv2.waitKey(0); cv2.destroyAllWindows()

        # Test 6: Open
        image, all_ok = aplly_image_effect(cv2_image.copy(), "Grayscale", size=None)
        image, all_ok = aplly_image_effect(image, "Otsu Threshold", size=None)
        image, all_ok = aplly_image_effect(image, "Open", size=7)
        cv2.imshow("FUNCTION 2: Test 6: aplly_image_effect - Open", image); cv2.waitKey(0); cv2.destroyAllWindows()

        # Test 7: Canny
        image, all_ok = aplly_image_effect(cv2_image.copy(), "Canny", size=None)
        cv2.imshow("FUNCTION 2: Test 7: aplly_image_effect - Canny", image); cv2.waitKey(0); cv2.destroyAllWindows()


# FUNCTION 3: text_comparator(image, coords_text_file_path:str, original_text_file_path:str, motor="Tesseract") -> float
def function3Test(apply:bool) -> None:
    if apply:
        image_file_path = "/home/sgarcia/Desktop/Global News/Proyects/OCR_ROC/scripts-de-prueba-ocr/ejemplos/Test/7_IMG.jpeg"
        cv2_image, _ = load_image_file(image_file_path)
        
        image_file_path = "/home/sgarcia/Desktop/Global News/Proyects/OCR_ROC/scripts-de-prueba-ocr/ejemplos/Test/7_IMG.jpeg"
        textCordDir = "/home/sgarcia/Desktop/Global News/Proyects/OCR_ROC/scripts-de-prueba-ocr/ejemplos/Test/7_COORD.txt"
        textDir = "/home/sgarcia/Desktop/Global News/Proyects/OCR_ROC/scripts-de-prueba-ocr/ejemplos/Test/7_TEXT.txt"
        ratio = text_comparator(cv2_image, textCordDir, textDir, "Tesseract")
        print(ratio)


# APPLY TESTS: ----------------------------------------------
function1Test(False)
function2Test(False)
function3Test(False)
