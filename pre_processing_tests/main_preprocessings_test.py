# 1. Create summary file:
import os
summary_file_path = "/home/sgarcia/Desktop/Global News/Proyects/OCR_ROC/scripts-de-prueba-ocr/pre_processing_tests"
os.chdir(summary_file_path)
log_file = open("pre-processing log.txt", 'w+')


# 2. Create the "all combinations matrix":
from effect_model import *
import preprocessings_combinations_summary as prepro

effect_matrix = prepro.get_all_combinations_effect_matrix()
log_file.write(f"Matrix has {len(effect_matrix.matrix)} rows.\n\n")
# for i, row in enumerate(effect_matrix.matrix): log_file.write(f" |--> Row {i}\thas {len(row)} effects: {row}.\n")
print(f"Matrix has {len(effect_matrix.matrix)} rows.")


# 3. Create summaries:
main_directory = "/home/sgarcia/Desktop/Global News/Proyects/OCR_ROC/scripts-de-prueba-ocr/ejemplos/Test"
best_n_combinations = 15

for file_index in [3, 4, 5, 7]:   
    prepro.get_preproseccing_summary(effect_matrix, main_directory, file_index, log_file, best_n_combinations, ocr_engine="Azure", print_results=False)


# 4. Close summary file:
log_file.write("\nEnd Program.\n")
log_file.close()
print('\a')