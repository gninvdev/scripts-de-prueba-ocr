Matrix has 324 rows.

 |--> Row 0	has 0 effects: [].
 |--> Row 1	has 1 effects: [Grayscale].
 |--> Row 2	has 1 effects: [Median Blurr-3].
 |--> Row 3	has 2 effects: [Grayscale, Median Blurr-3].
 |--> Row 4	has 1 effects: [Median Blurr-5].
 |--> Row 5	has 2 effects: [Grayscale, Median Blurr-5].
 |--> Row 6	has 1 effects: [Median Blurr-7].
 |--> Row 7	has 2 effects: [Grayscale, Median Blurr-7].
 |--> Row 8	has 1 effects: [Median Blurr-9].
 |--> Row 9	has 2 effects: [Grayscale, Median Blurr-9].
 |--> Row 10	has 1 effects: [Gaussian Blurr-3].
 |--> Row 11	has 2 effects: [Grayscale, Gaussian Blurr-3].
 |--> Row 12	has 1 effects: [Gaussian Blurr-5].
 |--> Row 13	has 2 effects: [Grayscale, Gaussian Blurr-5].
 |--> Row 14	has 1 effects: [Gaussian Blurr-7].
 |--> Row 15	has 2 effects: [Grayscale, Gaussian Blurr-7].
 |--> Row 16	has 1 effects: [Gaussian Blurr-9].
 |--> Row 17	has 2 effects: [Grayscale, Gaussian Blurr-9].
 |--> Row 18	has 2 effects: [Grayscale, Otsu Threshold].
 |--> Row 19	has 3 effects: [Grayscale, Median Blurr-3, Otsu Threshold].
 |--> Row 20	has 3 effects: [Grayscale, Median Blurr-5, Otsu Threshold].
 |--> Row 21	has 3 effects: [Grayscale, Median Blurr-7, Otsu Threshold].
 |--> Row 22	has 3 effects: [Grayscale, Median Blurr-9, Otsu Threshold].
 |--> Row 23	has 3 effects: [Grayscale, Gaussian Blurr-3, Otsu Threshold].
 |--> Row 24	has 3 effects: [Grayscale, Gaussian Blurr-5, Otsu Threshold].
 |--> Row 25	has 3 effects: [Grayscale, Gaussian Blurr-7, Otsu Threshold].
 |--> Row 26	has 3 effects: [Grayscale, Gaussian Blurr-9, Otsu Threshold].
 |--> Row 27	has 1 effects: [Open-1].
 |--> Row 28	has 2 effects: [Grayscale, Open-1].
 |--> Row 29	has 2 effects: [Median Blurr-3, Open-1].
 |--> Row 30	has 3 effects: [Grayscale, Median Blurr-3, Open-1].
 |--> Row 31	has 2 effects: [Median Blurr-5, Open-1].
 |--> Row 32	has 3 effects: [Grayscale, Median Blurr-5, Open-1].
 |--> Row 33	has 2 effects: [Median Blurr-7, Open-1].
 |--> Row 34	has 3 effects: [Grayscale, Median Blurr-7, Open-1].
 |--> Row 35	has 2 effects: [Median Blurr-9, Open-1].
 |--> Row 36	has 3 effects: [Grayscale, Median Blurr-9, Open-1].
 |--> Row 37	has 2 effects: [Gaussian Blurr-3, Open-1].
 |--> Row 38	has 3 effects: [Grayscale, Gaussian Blurr-3, Open-1].
 |--> Row 39	has 2 effects: [Gaussian Blurr-5, Open-1].
 |--> Row 40	has 3 effects: [Grayscale, Gaussian Blurr-5, Open-1].
 |--> Row 41	has 2 effects: [Gaussian Blurr-7, Open-1].
 |--> Row 42	has 3 effects: [Grayscale, Gaussian Blurr-7, Open-1].
 |--> Row 43	has 2 effects: [Gaussian Blurr-9, Open-1].
 |--> Row 44	has 3 effects: [Grayscale, Gaussian Blurr-9, Open-1].
 |--> Row 45	has 3 effects: [Grayscale, Otsu Threshold, Open-1].
 |--> Row 46	has 4 effects: [Grayscale, Median Blurr-3, Otsu Threshold, Open-1].
 |--> Row 47	has 4 effects: [Grayscale, Median Blurr-5, Otsu Threshold, Open-1].
 |--> Row 48	has 4 effects: [Grayscale, Median Blurr-7, Otsu Threshold, Open-1].
 |--> Row 49	has 4 effects: [Grayscale, Median Blurr-9, Otsu Threshold, Open-1].
 |--> Row 50	has 4 effects: [Grayscale, Gaussian Blurr-3, Otsu Threshold, Open-1].
 |--> Row 51	has 4 effects: [Grayscale, Gaussian Blurr-5, Otsu Threshold, Open-1].
 |--> Row 52	has 4 effects: [Grayscale, Gaussian Blurr-7, Otsu Threshold, Open-1].
 |--> Row 53	has 4 effects: [Grayscale, Gaussian Blurr-9, Otsu Threshold, Open-1].
 |--> Row 54	has 1 effects: [Open-3].
 |--> Row 55	has 2 effects: [Grayscale, Open-3].
 |--> Row 56	has 2 effects: [Median Blurr-3, Open-3].
 |--> Row 57	has 3 effects: [Grayscale, Median Blurr-3, Open-3].
 |--> Row 58	has 2 effects: [Median Blurr-5, Open-3].
 |--> Row 59	has 3 effects: [Grayscale, Median Blurr-5, Open-3].
 |--> Row 60	has 2 effects: [Median Blurr-7, Open-3].
 |--> Row 61	has 3 effects: [Grayscale, Median Blurr-7, Open-3].
 |--> Row 62	has 2 effects: [Median Blurr-9, Open-3].
 |--> Row 63	has 3 effects: [Grayscale, Median Blurr-9, Open-3].
 |--> Row 64	has 2 effects: [Gaussian Blurr-3, Open-3].
 |--> Row 65	has 3 effects: [Grayscale, Gaussian Blurr-3, Open-3].
 |--> Row 66	has 2 effects: [Gaussian Blurr-5, Open-3].
 |--> Row 67	has 3 effects: [Grayscale, Gaussian Blurr-5, Open-3].
 |--> Row 68	has 2 effects: [Gaussian Blurr-7, Open-3].
 |--> Row 69	has 3 effects: [Grayscale, Gaussian Blurr-7, Open-3].
 |--> Row 70	has 2 effects: [Gaussian Blurr-9, Open-3].
 |--> Row 71	has 3 effects: [Grayscale, Gaussian Blurr-9, Open-3].
 |--> Row 72	has 3 effects: [Grayscale, Otsu Threshold, Open-3].
 |--> Row 73	has 4 effects: [Grayscale, Median Blurr-3, Otsu Threshold, Open-3].
 |--> Row 74	has 4 effects: [Grayscale, Median Blurr-5, Otsu Threshold, Open-3].
 |--> Row 75	has 4 effects: [Grayscale, Median Blurr-7, Otsu Threshold, Open-3].
 |--> Row 76	has 4 effects: [Grayscale, Median Blurr-9, Otsu Threshold, Open-3].
 |--> Row 77	has 4 effects: [Grayscale, Gaussian Blurr-3, Otsu Threshold, Open-3].
 |--> Row 78	has 4 effects: [Grayscale, Gaussian Blurr-5, Otsu Threshold, Open-3].
 |--> Row 79	has 4 effects: [Grayscale, Gaussian Blurr-7, Otsu Threshold, Open-3].
 |--> Row 80	has 4 effects: [Grayscale, Gaussian Blurr-9, Otsu Threshold, Open-3].
 |--> Row 81	has 1 effects: [Open-5].
 |--> Row 82	has 2 effects: [Grayscale, Open-5].
 |--> Row 83	has 2 effects: [Median Blurr-3, Open-5].
 |--> Row 84	has 3 effects: [Grayscale, Median Blurr-3, Open-5].
 |--> Row 85	has 2 effects: [Median Blurr-5, Open-5].
 |--> Row 86	has 3 effects: [Grayscale, Median Blurr-5, Open-5].
 |--> Row 87	has 2 effects: [Median Blurr-7, Open-5].
 |--> Row 88	has 3 effects: [Grayscale, Median Blurr-7, Open-5].
 |--> Row 89	has 2 effects: [Median Blurr-9, Open-5].
 |--> Row 90	has 3 effects: [Grayscale, Median Blurr-9, Open-5].
 |--> Row 91	has 2 effects: [Gaussian Blurr-3, Open-5].
 |--> Row 92	has 3 effects: [Grayscale, Gaussian Blurr-3, Open-5].
 |--> Row 93	has 2 effects: [Gaussian Blurr-5, Open-5].
 |--> Row 94	has 3 effects: [Grayscale, Gaussian Blurr-5, Open-5].
 |--> Row 95	has 2 effects: [Gaussian Blurr-7, Open-5].
 |--> Row 96	has 3 effects: [Grayscale, Gaussian Blurr-7, Open-5].
 |--> Row 97	has 2 effects: [Gaussian Blurr-9, Open-5].
 |--> Row 98	has 3 effects: [Grayscale, Gaussian Blurr-9, Open-5].
 |--> Row 99	has 3 effects: [Grayscale, Otsu Threshold, Open-5].
 |--> Row 100	has 4 effects: [Grayscale, Median Blurr-3, Otsu Threshold, Open-5].
 |--> Row 101	has 4 effects: [Grayscale, Median Blurr-5, Otsu Threshold, Open-5].
 |--> Row 102	has 4 effects: [Grayscale, Median Blurr-7, Otsu Threshold, Open-5].
 |--> Row 103	has 4 effects: [Grayscale, Median Blurr-9, Otsu Threshold, Open-5].
 |--> Row 104	has 4 effects: [Grayscale, Gaussian Blurr-3, Otsu Threshold, Open-5].
 |--> Row 105	has 4 effects: [Grayscale, Gaussian Blurr-5, Otsu Threshold, Open-5].
 |--> Row 106	has 4 effects: [Grayscale, Gaussian Blurr-7, Otsu Threshold, Open-5].
 |--> Row 107	has 4 effects: [Grayscale, Gaussian Blurr-9, Otsu Threshold, Open-5].
 |--> Row 108	has 1 effects: [Open-7].
 |--> Row 109	has 2 effects: [Grayscale, Open-7].
 |--> Row 110	has 2 effects: [Median Blurr-3, Open-7].
 |--> Row 111	has 3 effects: [Grayscale, Median Blurr-3, Open-7].
 |--> Row 112	has 2 effects: [Median Blurr-5, Open-7].
 |--> Row 113	has 3 effects: [Grayscale, Median Blurr-5, Open-7].
 |--> Row 114	has 2 effects: [Median Blurr-7, Open-7].
 |--> Row 115	has 3 effects: [Grayscale, Median Blurr-7, Open-7].
 |--> Row 116	has 2 effects: [Median Blurr-9, Open-7].
 |--> Row 117	has 3 effects: [Grayscale, Median Blurr-9, Open-7].
 |--> Row 118	has 2 effects: [Gaussian Blurr-3, Open-7].
 |--> Row 119	has 3 effects: [Grayscale, Gaussian Blurr-3, Open-7].
 |--> Row 120	has 2 effects: [Gaussian Blurr-5, Open-7].
 |--> Row 121	has 3 effects: [Grayscale, Gaussian Blurr-5, Open-7].
 |--> Row 122	has 2 effects: [Gaussian Blurr-7, Open-7].
 |--> Row 123	has 3 effects: [Grayscale, Gaussian Blurr-7, Open-7].
 |--> Row 124	has 2 effects: [Gaussian Blurr-9, Open-7].
 |--> Row 125	has 3 effects: [Grayscale, Gaussian Blurr-9, Open-7].
 |--> Row 126	has 3 effects: [Grayscale, Otsu Threshold, Open-7].
 |--> Row 127	has 4 effects: [Grayscale, Median Blurr-3, Otsu Threshold, Open-7].
 |--> Row 128	has 4 effects: [Grayscale, Median Blurr-5, Otsu Threshold, Open-7].
 |--> Row 129	has 4 effects: [Grayscale, Median Blurr-7, Otsu Threshold, Open-7].
 |--> Row 130	has 4 effects: [Grayscale, Median Blurr-9, Otsu Threshold, Open-7].
 |--> Row 131	has 4 effects: [Grayscale, Gaussian Blurr-3, Otsu Threshold, Open-7].
 |--> Row 132	has 4 effects: [Grayscale, Gaussian Blurr-5, Otsu Threshold, Open-7].
 |--> Row 133	has 4 effects: [Grayscale, Gaussian Blurr-7, Otsu Threshold, Open-7].
 |--> Row 134	has 4 effects: [Grayscale, Gaussian Blurr-9, Otsu Threshold, Open-7].
 |--> Row 135	has 1 effects: [Open-9].
 |--> Row 136	has 2 effects: [Grayscale, Open-9].
 |--> Row 137	has 2 effects: [Median Blurr-3, Open-9].
 |--> Row 138	has 3 effects: [Grayscale, Median Blurr-3, Open-9].
 |--> Row 139	has 2 effects: [Median Blurr-5, Open-9].
 |--> Row 140	has 3 effects: [Grayscale, Median Blurr-5, Open-9].
 |--> Row 141	has 2 effects: [Median Blurr-7, Open-9].
 |--> Row 142	has 3 effects: [Grayscale, Median Blurr-7, Open-9].
 |--> Row 143	has 2 effects: [Median Blurr-9, Open-9].
 |--> Row 144	has 3 effects: [Grayscale, Median Blurr-9, Open-9].
 |--> Row 145	has 2 effects: [Gaussian Blurr-3, Open-9].
 |--> Row 146	has 3 effects: [Grayscale, Gaussian Blurr-3, Open-9].
 |--> Row 147	has 2 effects: [Gaussian Blurr-5, Open-9].
 |--> Row 148	has 3 effects: [Grayscale, Gaussian Blurr-5, Open-9].
 |--> Row 149	has 2 effects: [Gaussian Blurr-7, Open-9].
 |--> Row 150	has 3 effects: [Grayscale, Gaussian Blurr-7, Open-9].
 |--> Row 151	has 2 effects: [Gaussian Blurr-9, Open-9].
 |--> Row 152	has 3 effects: [Grayscale, Gaussian Blurr-9, Open-9].
 |--> Row 153	has 3 effects: [Grayscale, Otsu Threshold, Open-9].
 |--> Row 154	has 4 effects: [Grayscale, Median Blurr-3, Otsu Threshold, Open-9].
 |--> Row 155	has 4 effects: [Grayscale, Median Blurr-5, Otsu Threshold, Open-9].
 |--> Row 156	has 4 effects: [Grayscale, Median Blurr-7, Otsu Threshold, Open-9].
 |--> Row 157	has 4 effects: [Grayscale, Median Blurr-9, Otsu Threshold, Open-9].
 |--> Row 158	has 4 effects: [Grayscale, Gaussian Blurr-3, Otsu Threshold, Open-9].
 |--> Row 159	has 4 effects: [Grayscale, Gaussian Blurr-5, Otsu Threshold, Open-9].
 |--> Row 160	has 4 effects: [Grayscale, Gaussian Blurr-7, Otsu Threshold, Open-9].
 |--> Row 161	has 4 effects: [Grayscale, Gaussian Blurr-9, Otsu Threshold, Open-9].
 |--> Row 162	has 1 effects: [Canny].
 |--> Row 163	has 2 effects: [Grayscale, Canny].
 |--> Row 164	has 2 effects: [Median Blurr-3, Canny].
 |--> Row 165	has 3 effects: [Grayscale, Median Blurr-3, Canny].
 |--> Row 166	has 2 effects: [Median Blurr-5, Canny].
 |--> Row 167	has 3 effects: [Grayscale, Median Blurr-5, Canny].
 |--> Row 168	has 2 effects: [Median Blurr-7, Canny].
 |--> Row 169	has 3 effects: [Grayscale, Median Blurr-7, Canny].
 |--> Row 170	has 2 effects: [Median Blurr-9, Canny].
 |--> Row 171	has 3 effects: [Grayscale, Median Blurr-9, Canny].
 |--> Row 172	has 2 effects: [Gaussian Blurr-3, Canny].
 |--> Row 173	has 3 effects: [Grayscale, Gaussian Blurr-3, Canny].
 |--> Row 174	has 2 effects: [Gaussian Blurr-5, Canny].
 |--> Row 175	has 3 effects: [Grayscale, Gaussian Blurr-5, Canny].
 |--> Row 176	has 2 effects: [Gaussian Blurr-7, Canny].
 |--> Row 177	has 3 effects: [Grayscale, Gaussian Blurr-7, Canny].
 |--> Row 178	has 2 effects: [Gaussian Blurr-9, Canny].
 |--> Row 179	has 3 effects: [Grayscale, Gaussian Blurr-9, Canny].
 |--> Row 180	has 3 effects: [Grayscale, Otsu Threshold, Canny].
 |--> Row 181	has 4 effects: [Grayscale, Median Blurr-3, Otsu Threshold, Canny].
 |--> Row 182	has 4 effects: [Grayscale, Median Blurr-5, Otsu Threshold, Canny].
 |--> Row 183	has 4 effects: [Grayscale, Median Blurr-7, Otsu Threshold, Canny].
 |--> Row 184	has 4 effects: [Grayscale, Median Blurr-9, Otsu Threshold, Canny].
 |--> Row 185	has 4 effects: [Grayscale, Gaussian Blurr-3, Otsu Threshold, Canny].
 |--> Row 186	has 4 effects: [Grayscale, Gaussian Blurr-5, Otsu Threshold, Canny].
 |--> Row 187	has 4 effects: [Grayscale, Gaussian Blurr-7, Otsu Threshold, Canny].
 |--> Row 188	has 4 effects: [Grayscale, Gaussian Blurr-9, Otsu Threshold, Canny].
 |--> Row 189	has 2 effects: [Open-1, Canny].
 |--> Row 190	has 3 effects: [Grayscale, Open-1, Canny].
 |--> Row 191	has 3 effects: [Median Blurr-3, Open-1, Canny].
 |--> Row 192	has 4 effects: [Grayscale, Median Blurr-3, Open-1, Canny].
 |--> Row 193	has 3 effects: [Median Blurr-5, Open-1, Canny].
 |--> Row 194	has 4 effects: [Grayscale, Median Blurr-5, Open-1, Canny].
 |--> Row 195	has 3 effects: [Median Blurr-7, Open-1, Canny].
 |--> Row 196	has 4 effects: [Grayscale, Median Blurr-7, Open-1, Canny].
 |--> Row 197	has 3 effects: [Median Blurr-9, Open-1, Canny].
 |--> Row 198	has 4 effects: [Grayscale, Median Blurr-9, Open-1, Canny].
 |--> Row 199	has 3 effects: [Gaussian Blurr-3, Open-1, Canny].
 |--> Row 200	has 4 effects: [Grayscale, Gaussian Blurr-3, Open-1, Canny].
 |--> Row 201	has 3 effects: [Gaussian Blurr-5, Open-1, Canny].
 |--> Row 202	has 4 effects: [Grayscale, Gaussian Blurr-5, Open-1, Canny].
 |--> Row 203	has 3 effects: [Gaussian Blurr-7, Open-1, Canny].
 |--> Row 204	has 4 effects: [Grayscale, Gaussian Blurr-7, Open-1, Canny].
 |--> Row 205	has 3 effects: [Gaussian Blurr-9, Open-1, Canny].
 |--> Row 206	has 4 effects: [Grayscale, Gaussian Blurr-9, Open-1, Canny].
 |--> Row 207	has 4 effects: [Grayscale, Otsu Threshold, Open-1, Canny].
 |--> Row 208	has 5 effects: [Grayscale, Median Blurr-3, Otsu Threshold, Open-1, Canny].
 |--> Row 209	has 5 effects: [Grayscale, Median Blurr-5, Otsu Threshold, Open-1, Canny].
 |--> Row 210	has 5 effects: [Grayscale, Median Blurr-7, Otsu Threshold, Open-1, Canny].
 |--> Row 211	has 5 effects: [Grayscale, Median Blurr-9, Otsu Threshold, Open-1, Canny].
 |--> Row 212	has 5 effects: [Grayscale, Gaussian Blurr-3, Otsu Threshold, Open-1, Canny].
 |--> Row 213	has 5 effects: [Grayscale, Gaussian Blurr-5, Otsu Threshold, Open-1, Canny].
 |--> Row 214	has 5 effects: [Grayscale, Gaussian Blurr-7, Otsu Threshold, Open-1, Canny].
 |--> Row 215	has 5 effects: [Grayscale, Gaussian Blurr-9, Otsu Threshold, Open-1, Canny].
 |--> Row 216	has 2 effects: [Open-3, Canny].
 |--> Row 217	has 3 effects: [Grayscale, Open-3, Canny].
 |--> Row 218	has 3 effects: [Median Blurr-3, Open-3, Canny].
 |--> Row 219	has 4 effects: [Grayscale, Median Blurr-3, Open-3, Canny].
 |--> Row 220	has 3 effects: [Median Blurr-5, Open-3, Canny].
 |--> Row 221	has 4 effects: [Grayscale, Median Blurr-5, Open-3, Canny].
 |--> Row 222	has 3 effects: [Median Blurr-7, Open-3, Canny].
 |--> Row 223	has 4 effects: [Grayscale, Median Blurr-7, Open-3, Canny].
 |--> Row 224	has 3 effects: [Median Blurr-9, Open-3, Canny].
 |--> Row 225	has 4 effects: [Grayscale, Median Blurr-9, Open-3, Canny].
 |--> Row 226	has 3 effects: [Gaussian Blurr-3, Open-3, Canny].
 |--> Row 227	has 4 effects: [Grayscale, Gaussian Blurr-3, Open-3, Canny].
 |--> Row 228	has 3 effects: [Gaussian Blurr-5, Open-3, Canny].
 |--> Row 229	has 4 effects: [Grayscale, Gaussian Blurr-5, Open-3, Canny].
 |--> Row 230	has 3 effects: [Gaussian Blurr-7, Open-3, Canny].
 |--> Row 231	has 4 effects: [Grayscale, Gaussian Blurr-7, Open-3, Canny].
 |--> Row 232	has 3 effects: [Gaussian Blurr-9, Open-3, Canny].
 |--> Row 233	has 4 effects: [Grayscale, Gaussian Blurr-9, Open-3, Canny].
 |--> Row 234	has 4 effects: [Grayscale, Otsu Threshold, Open-3, Canny].
 |--> Row 235	has 5 effects: [Grayscale, Median Blurr-3, Otsu Threshold, Open-3, Canny].
 |--> Row 236	has 5 effects: [Grayscale, Median Blurr-5, Otsu Threshold, Open-3, Canny].
 |--> Row 237	has 5 effects: [Grayscale, Median Blurr-7, Otsu Threshold, Open-3, Canny].
 |--> Row 238	has 5 effects: [Grayscale, Median Blurr-9, Otsu Threshold, Open-3, Canny].
 |--> Row 239	has 5 effects: [Grayscale, Gaussian Blurr-3, Otsu Threshold, Open-3, Canny].
 |--> Row 240	has 5 effects: [Grayscale, Gaussian Blurr-5, Otsu Threshold, Open-3, Canny].
 |--> Row 241	has 5 effects: [Grayscale, Gaussian Blurr-7, Otsu Threshold, Open-3, Canny].
 |--> Row 242	has 5 effects: [Grayscale, Gaussian Blurr-9, Otsu Threshold, Open-3, Canny].
 |--> Row 243	has 2 effects: [Open-5, Canny].
 |--> Row 244	has 3 effects: [Grayscale, Open-5, Canny].
 |--> Row 245	has 3 effects: [Median Blurr-3, Open-5, Canny].
 |--> Row 246	has 4 effects: [Grayscale, Median Blurr-3, Open-5, Canny].
 |--> Row 247	has 3 effects: [Median Blurr-5, Open-5, Canny].
 |--> Row 248	has 4 effects: [Grayscale, Median Blurr-5, Open-5, Canny].
 |--> Row 249	has 3 effects: [Median Blurr-7, Open-5, Canny].
 |--> Row 250	has 4 effects: [Grayscale, Median Blurr-7, Open-5, Canny].
 |--> Row 251	has 3 effects: [Median Blurr-9, Open-5, Canny].
 |--> Row 252	has 4 effects: [Grayscale, Median Blurr-9, Open-5, Canny].
 |--> Row 253	has 3 effects: [Gaussian Blurr-3, Open-5, Canny].
 |--> Row 254	has 4 effects: [Grayscale, Gaussian Blurr-3, Open-5, Canny].
 |--> Row 255	has 3 effects: [Gaussian Blurr-5, Open-5, Canny].
 |--> Row 256	has 4 effects: [Grayscale, Gaussian Blurr-5, Open-5, Canny].
 |--> Row 257	has 3 effects: [Gaussian Blurr-7, Open-5, Canny].
 |--> Row 258	has 4 effects: [Grayscale, Gaussian Blurr-7, Open-5, Canny].
 |--> Row 259	has 3 effects: [Gaussian Blurr-9, Open-5, Canny].
 |--> Row 260	has 4 effects: [Grayscale, Gaussian Blurr-9, Open-5, Canny].
 |--> Row 261	has 4 effects: [Grayscale, Otsu Threshold, Open-5, Canny].
 |--> Row 262	has 5 effects: [Grayscale, Median Blurr-3, Otsu Threshold, Open-5, Canny].
 |--> Row 263	has 5 effects: [Grayscale, Median Blurr-5, Otsu Threshold, Open-5, Canny].
 |--> Row 264	has 5 effects: [Grayscale, Median Blurr-7, Otsu Threshold, Open-5, Canny].
 |--> Row 265	has 5 effects: [Grayscale, Median Blurr-9, Otsu Threshold, Open-5, Canny].
 |--> Row 266	has 5 effects: [Grayscale, Gaussian Blurr-3, Otsu Threshold, Open-5, Canny].
 |--> Row 267	has 5 effects: [Grayscale, Gaussian Blurr-5, Otsu Threshold, Open-5, Canny].
 |--> Row 268	has 5 effects: [Grayscale, Gaussian Blurr-7, Otsu Threshold, Open-5, Canny].
 |--> Row 269	has 5 effects: [Grayscale, Gaussian Blurr-9, Otsu Threshold, Open-5, Canny].
 |--> Row 270	has 2 effects: [Open-7, Canny].
 |--> Row 271	has 3 effects: [Grayscale, Open-7, Canny].
 |--> Row 272	has 3 effects: [Median Blurr-3, Open-7, Canny].
 |--> Row 273	has 4 effects: [Grayscale, Median Blurr-3, Open-7, Canny].
 |--> Row 274	has 3 effects: [Median Blurr-5, Open-7, Canny].
 |--> Row 275	has 4 effects: [Grayscale, Median Blurr-5, Open-7, Canny].
 |--> Row 276	has 3 effects: [Median Blurr-7, Open-7, Canny].
 |--> Row 277	has 4 effects: [Grayscale, Median Blurr-7, Open-7, Canny].
 |--> Row 278	has 3 effects: [Median Blurr-9, Open-7, Canny].
 |--> Row 279	has 4 effects: [Grayscale, Median Blurr-9, Open-7, Canny].
 |--> Row 280	has 3 effects: [Gaussian Blurr-3, Open-7, Canny].
 |--> Row 281	has 4 effects: [Grayscale, Gaussian Blurr-3, Open-7, Canny].
 |--> Row 282	has 3 effects: [Gaussian Blurr-5, Open-7, Canny].
 |--> Row 283	has 4 effects: [Grayscale, Gaussian Blurr-5, Open-7, Canny].
 |--> Row 284	has 3 effects: [Gaussian Blurr-7, Open-7, Canny].
 |--> Row 285	has 4 effects: [Grayscale, Gaussian Blurr-7, Open-7, Canny].
 |--> Row 286	has 3 effects: [Gaussian Blurr-9, Open-7, Canny].
 |--> Row 287	has 4 effects: [Grayscale, Gaussian Blurr-9, Open-7, Canny].
 |--> Row 288	has 4 effects: [Grayscale, Otsu Threshold, Open-7, Canny].
 |--> Row 289	has 5 effects: [Grayscale, Median Blurr-3, Otsu Threshold, Open-7, Canny].
 |--> Row 290	has 5 effects: [Grayscale, Median Blurr-5, Otsu Threshold, Open-7, Canny].
 |--> Row 291	has 5 effects: [Grayscale, Median Blurr-7, Otsu Threshold, Open-7, Canny].
 |--> Row 292	has 5 effects: [Grayscale, Median Blurr-9, Otsu Threshold, Open-7, Canny].
 |--> Row 293	has 5 effects: [Grayscale, Gaussian Blurr-3, Otsu Threshold, Open-7, Canny].
 |--> Row 294	has 5 effects: [Grayscale, Gaussian Blurr-5, Otsu Threshold, Open-7, Canny].
 |--> Row 295	has 5 effects: [Grayscale, Gaussian Blurr-7, Otsu Threshold, Open-7, Canny].
 |--> Row 296	has 5 effects: [Grayscale, Gaussian Blurr-9, Otsu Threshold, Open-7, Canny].
 |--> Row 297	has 2 effects: [Open-9, Canny].
 |--> Row 298	has 3 effects: [Grayscale, Open-9, Canny].
 |--> Row 299	has 3 effects: [Median Blurr-3, Open-9, Canny].
 |--> Row 300	has 4 effects: [Grayscale, Median Blurr-3, Open-9, Canny].
 |--> Row 301	has 3 effects: [Median Blurr-5, Open-9, Canny].
 |--> Row 302	has 4 effects: [Grayscale, Median Blurr-5, Open-9, Canny].
 |--> Row 303	has 3 effects: [Median Blurr-7, Open-9, Canny].
 |--> Row 304	has 4 effects: [Grayscale, Median Blurr-7, Open-9, Canny].
 |--> Row 305	has 3 effects: [Median Blurr-9, Open-9, Canny].
 |--> Row 306	has 4 effects: [Grayscale, Median Blurr-9, Open-9, Canny].
 |--> Row 307	has 3 effects: [Gaussian Blurr-3, Open-9, Canny].
 |--> Row 308	has 4 effects: [Grayscale, Gaussian Blurr-3, Open-9, Canny].
 |--> Row 309	has 3 effects: [Gaussian Blurr-5, Open-9, Canny].
 |--> Row 310	has 4 effects: [Grayscale, Gaussian Blurr-5, Open-9, Canny].
 |--> Row 311	has 3 effects: [Gaussian Blurr-7, Open-9, Canny].
 |--> Row 312	has 4 effects: [Grayscale, Gaussian Blurr-7, Open-9, Canny].
 |--> Row 313	has 3 effects: [Gaussian Blurr-9, Open-9, Canny].
 |--> Row 314	has 4 effects: [Grayscale, Gaussian Blurr-9, Open-9, Canny].
 |--> Row 315	has 4 effects: [Grayscale, Otsu Threshold, Open-9, Canny].
 |--> Row 316	has 5 effects: [Grayscale, Median Blurr-3, Otsu Threshold, Open-9, Canny].
 |--> Row 317	has 5 effects: [Grayscale, Median Blurr-5, Otsu Threshold, Open-9, Canny].
 |--> Row 318	has 5 effects: [Grayscale, Median Blurr-7, Otsu Threshold, Open-9, Canny].
 |--> Row 319	has 5 effects: [Grayscale, Median Blurr-9, Otsu Threshold, Open-9, Canny].
 |--> Row 320	has 5 effects: [Grayscale, Gaussian Blurr-3, Otsu Threshold, Open-9, Canny].
 |--> Row 321	has 5 effects: [Grayscale, Gaussian Blurr-5, Otsu Threshold, Open-9, Canny].
 |--> Row 322	has 5 effects: [Grayscale, Gaussian Blurr-7, Otsu Threshold, Open-9, Canny].
 |--> Row 323	has 5 effects: [Grayscale, Gaussian Blurr-9, Otsu Threshold, Open-9, Canny].

File 3 process starts at: 2021-11-23 14:56:38.464995.

Original image has a ratio of: 97.09261956697692 %.


The best 15 are:
1.	98.4879 % --> [Grayscale, Gaussian Blurr-9, Otsu Threshold, Open-3].
2.	98.30524 % --> [Grayscale, Gaussian Blurr-7, Otsu Threshold, Open-3].
3.	98.16545 % --> [Median Blurr-7].
4.	98.16545 % --> [Median Blurr-7, Open-1].
5.	98.12629 % --> [Grayscale, Median Blurr-5, Open-3].
6.	98.08646 % --> [Median Blurr-7, Open-3].
7.	98.03958 % --> [Median Blurr-5, Open-3].
8.	98.02824 % --> [Grayscale, Gaussian Blurr-9, Otsu Threshold].
9.	98.02824 % --> [Grayscale, Gaussian Blurr-9, Otsu Threshold, Open-1].
10.	98.02156 % --> [Median Blurr-7, Open-5].
11.	97.98474 % --> [Grayscale, Median Blurr-7, Open-5].
12.	97.9749 % --> [Grayscale, Gaussian Blurr-9, Open-3].
13.	97.97296 % --> [Grayscale, Gaussian Blurr-5, Otsu Threshold, Open-3].
14.	97.92196 % --> [Grayscale, Median Blurr-7, Open-3].
15.	97.85725 % --> [Grayscale, Median Blurr-7].

Process ends at: 2021-11-23 15:32:04.987544.

Process takes 35 minutes.

------------------------------


File 4 process starts at: 2021-11-23 15:32:04.993004.

Original image has a ratio of: 95.21741536260119 %.


The best 15 are:
1.	96.15477 % --> [Grayscale, Gaussian Blurr-5, Otsu Threshold].
2.	96.15477 % --> [Grayscale, Gaussian Blurr-5, Otsu Threshold, Open-1].
3.	96.02891 % --> [Grayscale, Gaussian Blurr-5].
4.	96.02891 % --> [Grayscale, Gaussian Blurr-5, Open-1].
5.	95.84832 % --> [Gaussian Blurr-5].
6.	95.84832 % --> [Gaussian Blurr-5, Open-1].
7.	95.83995 % --> [Gaussian Blurr-3].
8.	95.83995 % --> [Gaussian Blurr-3, Open-1].
9.	95.6406 % --> [Grayscale, Gaussian Blurr-7, Otsu Threshold].
10.	95.6406 % --> [Grayscale, Gaussian Blurr-7, Otsu Threshold, Open-1].
11.	95.57284 % --> [Grayscale, Gaussian Blurr-7].
12.	95.57284 % --> [Grayscale, Gaussian Blurr-7, Open-1].
13.	95.37742 % --> [Grayscale, Gaussian Blurr-3, Otsu Threshold].
14.	95.37742 % --> [Grayscale, Gaussian Blurr-3, Otsu Threshold, Open-1].
15.	95.37513 % --> [Grayscale, Median Blurr-3, Open-3].

Process ends at: 2021-11-23 15:58:57.141002.

Process takes 26 minutes.

------------------------------


File 5 process starts at: 2021-11-23 15:58:57.143321.

Original image has a ratio of: 98.8048721474565 %.


The best 15 are:
1.	98.83747 % --> [Grayscale].
2.	98.83747 % --> [Grayscale, Open-1].
3.	98.83175 % --> [Gaussian Blurr-3].
4.	98.83175 % --> [Gaussian Blurr-3, Open-1].
5.	98.80487 % --> [].
6.	98.80487 % --> [Open-1].
7.	98.78848 % --> [Grayscale, Median Blurr-3].
8.	98.78848 % --> [Grayscale, Median Blurr-3, Open-1].
9.	98.76242 % --> [Grayscale, Gaussian Blurr-3].
10.	98.76242 % --> [Grayscale, Gaussian Blurr-3, Open-1].
11.	98.74015 % --> [Gaussian Blurr-7].
12.	98.74015 % --> [Gaussian Blurr-7, Open-1].
13.	98.71874 % --> [Grayscale, Gaussian Blurr-5].
14.	98.71874 % --> [Grayscale, Gaussian Blurr-5, Open-1].
15.	98.71328 % --> [Median Blurr-3].

Process ends at: 2021-11-23 16:21:52.312611.

Process takes 22 minutes.

------------------------------


File 7 process starts at: 2021-11-23 16:21:52.314893.

Original image has a ratio of: 96.71411947531425 %.


The best 15 are:
1.	96.71412 % --> [].
2.	96.71412 % --> [Open-1].
3.	96.63476 % --> [Grayscale, Gaussian Blurr-5, Otsu Threshold].
4.	96.63476 % --> [Grayscale, Gaussian Blurr-5, Otsu Threshold, Open-1].
5.	96.58523 % --> [Grayscale, Median Blurr-3].
6.	96.58523 % --> [Grayscale, Median Blurr-3, Open-1].
7.	96.57822 % --> [Median Blurr-3].
8.	96.57822 % --> [Median Blurr-3, Open-1].
9.	96.57658 % --> [Grayscale, Gaussian Blurr-5].
10.	96.57658 % --> [Grayscale, Gaussian Blurr-5, Open-1].
11.	96.56986 % --> [Grayscale, Gaussian Blurr-3, Otsu Threshold].
12.	96.56986 % --> [Grayscale, Gaussian Blurr-3, Otsu Threshold, Open-1].
13.	96.53709 % --> [Grayscale, Median Blurr-3, Open-3].
14.	96.51723 % --> [Grayscale, Gaussian Blurr-3].
15.	96.51723 % --> [Grayscale, Gaussian Blurr-3, Open-1].

Process ends at: 2021-11-23 16:47:37.262081.

Process takes 25 minutes.

------------------------------


End Program.
