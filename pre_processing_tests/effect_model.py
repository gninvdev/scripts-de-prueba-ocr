from typing import List
import sys


# Example: Gaussian Blurr
class Effect:
    
    def __init__(self, name: str, size=None) -> None:
        if (name is None) or (name == ''):
            sys.exit('Effect should have a name!')
        else:
            self.name = name 
        self.size = size
    
    
    def __str__(self) -> str:
        return self.name
    
    
    def __repr__(self) -> str:
        return self.name + '-' + str(self.size) if self.size is not None else self.name
    
    
    def getInfo(self) -> None:
        print(f'Effect: {self.name}.')
        if self.size is not None: print(f'Size: {self.size}.')


    def prettyName(self) -> str:
        return self.name + '-' + str(self.size) if self.size is not None else self.name


# Example: Blurr -> variants= 'Gaussian', 'Median'
class EffectFamily:
    
    def __init__(self, name: str, variants=[], sizes=[]) -> None:
        if (name is None) or (name == ''):
            sys.exit('Effect family should have a name!')
        else:
            self.name = name
        self.variants = variants if variants is not None else []
        self.sizes = sizes if sizes is not None else []
        print(f"Effect Family '{self.name}' created correctly.")
    
    
    def getInfo(self) -> None:
        print(f'Effect: {self.name}.')
        if self.variants != []: print(f'Variants: {self.variants}.')
        if self.sizes != []: print(f'Sizes: {self.sizes}.')
    
    
    def combinations(self) -> List[Effect]:
        if (self.variants == []) and (self.sizes == []):
            return [Effect(self.name, size=None)]
        elif (self.variants == []) and (self.sizes != []):
            return [Effect(name=self.name, size=s) for s in self.sizes]
        elif (self.variants != []) and (self.sizes == []):
            return [Effect(name=var_name + ' ' + self.name, size=None) for var_name in self.variants]
        else:
            return [Effect(name=var_name + ' ' + self.name, size=s) for var_name in self.variants for s in self.sizes]
    
    
    def printCombinations(self) -> None:
        comb = self.combinations()
        comb_names = []
        for eff in comb: comb_names.append(eff.prettyName())
        print(comb_names)



# Sera una Matriz (lista de listas) de efectos.
# [ [Effect_1],
#   [Effect_1, Effect_2-A]
#   [Effect_1, Effect_2-B] ]
class EffectsMatrix:
    
    def __init__(self) -> None:
        self.matrix = [[]] # List[List[Effect]]
        print("Effects matrix created correctly.")
    
    
    def getInfo(self) -> None:
        print(f"Matrix has: {len(self.matrix)} row/s.")
        for i, row in enumerate(self.matrix): print(f" |--> Row {i+1} has {len(row)} effects.")
    
    
    def getStringInfo(self) -> List[str]:
        information = []
        for i, row in enumerate(self.matrix):
            information.append(f" |--> Row {i} has {len(row)} effects.")
            return information
    
    
    def printMatrix(self) -> None:
        print('[')
        for effects_list in self.matrix:
            effects_names = [eff.prettyName() for eff in effects_list]
            print(f"  {effects_names}")
        print(']')
    
    
    def addEffectToMatrix(self, input_matrix: List[List[Effect]], effect: Effect, predecessor=None) -> List[List[Effect]]:
        new_matrix = [[]]
        for effects_list in input_matrix:
            if (predecessor is None) or (predecessor in effects_list):
                row = effects_list.copy()
                row.append(effect)
                new_matrix.append(row)
        
        matrix = input_matrix.copy()
        for r in new_matrix[1:]: matrix.append(r)
        return matrix
    
    
    def addEffect(self, effect: Effect, predecessor=None) -> None:
        self.matrix = self.addEffectToMatrix(self.matrix, effect, predecessor)
        print(f"Effect '{effect.prettyName()}' added correctly.")

    
    def addEffectFamily(self, effect_family: EffectFamily, predecessor=None) -> None:
    # Funcion que crea las listas:
    # EXISTING: matrix_effect = [[<previous_effects>]]
    # INPUT: matrix_effect.add(EffectFamily(name="Effect", variants=['A', 'B'], sizes=[1, 2, 3]))
    # OUTPUT: [ [<previous_effects>],
    #           [<previous_effects> , 'Effect-A-1'],
    #           [<previous_effects> , 'Effect-A-2'],
    #           [<previous_effects> , 'Effect-A-3'],
    #           [<previous_effects> , 'Effect-B-1'],
    #           [<previous_effects> , 'Effect-B-2'],
    #           [<previous_effects> , 'Effect-B-3'], ]
        family_combinations = effect_family.combinations()
        
        new_matrix = [[[]]]
        for effect in family_combinations:
            matrix = self.addEffectToMatrix(self.matrix, effect, predecessor)
            new_matrix.append(matrix)

        # new_matrix = [ m_1, m_2] / m_x = [ [], [Eff_1] ] / r = [Eff_1]
        original_size = len(self.matrix)
        for m in new_matrix[1:]:
            for r in m[original_size:]:
                self.matrix.append(r)



# ----------------------------- TESTS ---------------------------------
# EFFECT: -------------------------------------------------------------
def effectTests(apply):
    if apply:

        # TEST 0: [effect='', size=[]] or [effect=None, size=[]]
        # eff_0 = Effect(name='', size=None)
        # eff_0 = Effect(name=None, size=None)

        # TEST 1: [effect='Effect_1', size=[]]
        eff_1 = Effect('Effect_1', size=None)
        # eff_1.getInfo()
        print(eff_1.prettyName())

        # TEST 2: [effect='Effect_2', size=1]
        eff_2 = Effect('Effect_2', size=1)
        # eff_2.getInfo()
        print(eff_2.prettyName())


# EFFECT FAMILY: ------------------------------------------------------
def effectsFamilyTests(apply):
    if apply:

        # TEST 0: [effect='', variants=[], sizes=[]] or [effect=None, variants=[], sizes=[]]
        # fam_0 = EffectFamily(name='', variants=None, sizes=None)
        # fam_0 = EffectFamily(name=None, variants=None, sizes=None)

        # TEST 1: [effect='Effect_1', variants=[], sizes=[]]
        fam_1 = EffectFamily('Effect_1', variants=None, sizes=None)
        fam_1.getInfo()
        # print(fam_1.combinations())
        fam_1.printCombinations()

        # TEST 2: [effect='Effect_2', variants=['A', 'B'], sizes=[]]
        fam_2 = EffectFamily('Effect_2', variants=['A', 'B'], sizes=None)
        fam_2.getInfo()
        # print(fam_2.combinations())
        fam_2.printCombinations()
        
        # TEST 3: [effect='Effect_3', variants=[], sizes=[1, 2]]
        fam_3 = EffectFamily('Effect_3', variants=None, sizes=[1, 2])
        fam_3.getInfo()
        # print(fam_2.combinations())
        fam_3.printCombinations()
        
        # TEST 4: [effect='Effect_4', variants=['A', 'B'], sizes=[1, 2]]
        fam_4 = EffectFamily('Effect_4', variants=['A', 'B'], sizes=[1, 2, 3])
        fam_4.getInfo()
        # print(fam_2.combinations())
        fam_4.printCombinations()


# EFFECT MATRIX: ------------------------------------------------------
def effectsMatrixTests(apply_simple, apply_complex):
    if apply_simple:
        # SIMPLE EFFECT TESTS:
        mtx_1 = EffectsMatrix()
        mtx_1.getInfo()
        
        # TEST 1: Add 1 Effect.
        eff_1 = Effect('Effect_1', size=None)
        mtx_1.addEffect(eff_1)
        mtx_1.getInfo()
        mtx_1.printMatrix()
        
        # TEST 2: Add other Effect.
        eff_2 = Effect('Effect_2', size=1)
        mtx_1.addEffect(eff_2)
        mtx_1.getInfo()
        mtx_1.printMatrix()
        
    if apply_complex:
        # FAMILY EFFECTS TESTS:
        mtx_1 = EffectsMatrix()
        mtx_1.getInfo()
        
        # TEST 1: Add 1 simple EffectFamily (one Effect).
        fam_1 = EffectFamily('Effect_1', variants=None, sizes=None)
        mtx_1.addEffectFamily(fam_1)
        mtx_1.getInfo()
        mtx_1.printMatrix()
        
        # TEST 2: Add a EffectFamily.
        fam_2 = EffectFamily('Effect_1', variants=['A', 'B'], sizes=[1, 2])
        mtx_1.addEffectFamily(fam_2)
        mtx_1.getInfo()
        mtx_1.printMatrix()


# APPLY TESTS: --------------------------------------------------------
# effectTests(False)
# effectsFamilyTests(False)
# effectsMatrixTests(False, False)