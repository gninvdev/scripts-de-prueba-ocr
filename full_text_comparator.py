"""
    full_text_comparator.py

    Módulo de comparación por texto completo y línea por línea.

"""

import os
import difflib
from typing import Tuple

def full_text_compare(text1: str, text2: str, see_diff: bool = False):
    """
        Devuelve el ratio de comparación entre dos textos completos

        ### Parameters
        text1, text2 : str
            - Textos a comparar

        see_diff : bool, (default False)
            - Si es True genera un html con las diferencias marcadas

        ### Returns
        float
            - Ratio de comparación
    """

    m = difflib.SequenceMatcher(None, text1, text2)

    if see_diff:
        html = difflib.HtmlDiff()
        differences = html.make_file(list(text1.splitlines(True)), list(
            text2.splitlines(True)), "Original", "OCR")
        with open(os.path.abspath('diff.html'), "w+", encoding="utf-8") as fp:
            fp.write(differences)

    return round(m.ratio() * 100, 3)


def find_overlapping_boxes(og_coord: Tuple[int, int, int, int], data: dict):
    """
        Encuentra todos los rectángulos que solapan al original en una lista

        ### Parameters
        og_coord : tuple[int, int, int, int]
            Rectángulo original

        data : dict
            Diccionario de coordenadas
            - Keys:
                - coord : list[tuple[int, int, int, int]]
                    - Lista de coordenadas
                - text : list[str]
                    - Lista de texto

        ### Returns
        index_list : list[int]
            Lista de índices de rectángulos que se solapan en la lista de coordenadas
    """

    index_list: list = []

    n_boxes: int = len(data['coord'])

    for i in range(n_boxes):

        coord = data['coord'][i]

        if not (og_coord[0] >= coord[2] or og_coord[2] <= coord[0] or og_coord[3] <= coord[1] or og_coord[1] >= coord[3]):
            index_list.append(i)

    return index_list


def google_get_text_within(og_coord: Tuple[int, int, int, int], document):
    from google.cloud import vision
    """
        Encuentra el texto en los rectángulos que solapan al original en el json de Google

        ### Parameters
        og_coord : tuple[int, int, int, int]
            Rectángulo original

        document : dict
            Diccionario de coordenadas de Google

        ### Returns
        text : string
            Texto entre las coordenadas
    """

    text = ""

    (x1, y1, x2, y2) = og_coord

    breaktypes = vision.TextAnnotation.DetectedBreak.BreakType

    for page in document.pages:
        for block in page.blocks:
            for paragraph in block.paragraphs:
                for word in paragraph.words:
                    for symbol in word.symbols:
                        # Obtiene el rectángulo más chico que incluye al símbolo
                        sorted_x = [symbol.bounding_box.vertices[0].x, symbol.bounding_box.vertices[1].x,
                                    symbol.bounding_box.vertices[2].x, symbol.bounding_box.vertices[3].x]
                        sorted_y = [symbol.bounding_box.vertices[0].y, symbol.bounding_box.vertices[1].y,
                                    symbol.bounding_box.vertices[2].y, symbol.bounding_box.vertices[3].y]
                        sorted_x.sort()
                        sorted_y.sort()
                        min_x = sorted_x[1]
                        max_x = sorted_x[2]
                        min_y = sorted_y[1]
                        max_y = sorted_y[2]
                        if not (x1 >= max_x or x2 <= min_x or y2 <= min_y or y1 >= max_y):
                            text += symbol.text
                            if(symbol.property.detected_break.type_ in [breaktypes.EOL_SURE_SPACE, breaktypes.LINE_BREAK]):
                                return text
                            if(symbol.property.detected_break.type_ in [breaktypes.HYPHEN]):
                                text += "-"
                                return text
                            if(symbol.property.detected_break.type_ in [breaktypes.SPACE]):
                                text += ' '
                            if(symbol.property.detected_break.type_ in [breaktypes.SURE_SPACE]):
                                text += '\t'

    return text


def easy_get_text_within(og_coord: Tuple[int, int, int, int], data: dict):
    """
        Encuentra el texto en los rectángulos que solapan al original en el diccionario del EasyOCR

        ### Parameters
        og_coord : tuple[int, int, int, int]
            Rectángulo original

        document : dict
            Diccionario de coordenadas de EasyOCR

        ### Returns
        text : string
            Texto entre las coordenadas
    """

    n_boxes: int = len(data['coord'])

    text = ""

    for i in range(n_boxes):

        coord = data['coord'][i]

        og_lx = og_coord[0]
        og_ty = og_coord[1]
        og_rx = og_coord[2]
        og_by = og_coord[3]
        ocr_lx = coord[0]
        ocr_ty = coord[1]
        ocr_rx = coord[2]
        ocr_by = coord[3]

        area_og = (og_rx - og_lx) * (og_by - og_ty)
        area_ocr = (ocr_rx - ocr_lx) * (ocr_by - ocr_ty)

        if not (og_lx >= ocr_rx or og_rx <= ocr_lx or og_by <= ocr_ty or og_ty >= ocr_by):
            # Calcula el área y porcentaje de overlap entre los dos rectángulos
            overlap = (max(og_lx, ocr_lx) - min(og_rx, ocr_rx)) * \
                (max(og_ty, ocr_ty) - min(og_by, ocr_by))

            try:
                percentage_ocr = overlap/(area_ocr)
            except Exception as e:
                print(e)
                percentage_ocr = 0

            try:
                percentage_og = overlap/(area_og)
            except Exception as e:
                print(e)
                percentage_ocr = 0

            if(max(percentage_ocr, percentage_og) >= 0.2):
                text += data['text'][i] + " "
            print([data['text'][i], coord, percentage_ocr, percentage_og])

    return text


def compare_line(original_line: str, ocr_line: str) -> float:
    """
        Devuelve el ratio de comparación de dos líneas de texto

        ### Parameters
        original_line : str
            Texto original

        ocr_line : str
            Texto a comparar

        ### Returns
        ratio : float
            Ratio de comparación en porcentaje (redondeado a 5 cifras significativas)
    """

    m = difflib.SequenceMatcher(None, original_line, ocr_line)

    return round(m.ratio() * 100, 5)


def compare_line_data(original_data: dict, ocr_data: dict, ocr: str = "tesseract", to_file=False) -> float:
    """
        Devuelve el ratio de comparación entre dos diccionarios de datos.
        Compara línea por línea según las coordenadas y devuelve el promedio de ratios

        ### Parameters
        original_data : dict
            Diccionario de coordenadas del original
            - Keys:
                - coord : list[tuple[int, int, int, int]]
                    - Lista de coordenadas
                - text : list[str]
                    - Lista de texto

        ocr_data : dict
            Diccionario de coordenadas del ocr
            - Keys:
                - coord : list[tuple[int, int, int, int]]
                    - Lista de coordenadas
                - text : list[str]
                    - Lista de texto

        to_file : bool, (default False)
            Si es True genera un txt con la comparación

        ### Returns
        float
            Ratio de comparación
    """

    n_boxes: int = len(original_data['coord'])
    overall_ratios: list = []
    character_count = []

    file_text: str = ""

    if ocr == "tesseract" or ocr == "abbyy":
        for i in range(n_boxes):

            print(str(i) + "/" + str(n_boxes), end='\r')

            closest_index: int = -1

            overlap: list = find_overlapping_boxes(
                original_data['coord'][i], ocr_data)

            original_line = original_data['text'][i]
            character_count.append(len(original_line))
            line_ratios: list = []

            if not overlap:
                overall_ratios.append(
                    0.0)  # Si no encuentra un match, agrega un ratio = 0.0%
            else:
                for index in overlap:
                    line_ratios.append(compare_line(
                        original_line.rstrip(), ocr_data['text'][index].rstrip()))
                # Si hay más de un rectángulo que solapa, elije el que tiene mejor ratio
                overall_ratios.append(max(line_ratios))
                closest_index = overlap[max(
                    enumerate(line_ratios), key=lambda x: x[1])[0]]  # Hace un argmax

            if to_file:

                file_text += "CO> " + str(original_data['coord'][i]) + " -> "
                if closest_index != -1:
                    file_text += str(ocr_data['coord'][closest_index])
                else:
                    file_text += "NO MATCH"
                file_text += "\nOG> " + original_line.rstrip()
                file_text += "\nOC> "
                if closest_index != -1:
                    file_text += ocr_data['text'][closest_index].rstrip() + \
                        "\nRA> " + str(max(line_ratios)) + "%\n"
                else:
                    file_text += "NO MATCH" + "\nRA> 0.0%\n"
    elif ocr == "google":
        for i in range(n_boxes):

            print(str(i) + "/" + str(n_boxes), end='\r')

            original_line = original_data['text'][i]
            character_count.append(len(original_line))

            ocr_line = google_get_text_within(
                original_data['coord'][i], ocr_data)

            ratio = compare_line(original_line.rstrip(), ocr_line.rstrip())
            overall_ratios.append(ratio)

            if to_file:

                file_text += "CO> " + str(original_data['coord'][i])
                file_text += "\nOG> " + original_line.rstrip()
                file_text += "\nOC> "
                file_text += ocr_line.rstrip() + \
                    "\nRA> " + str(ratio) + "%\n"
    elif ocr == "easyocr" or ocr == "azure":
        for i in range(n_boxes):

            print(str(i) + "/" + str(n_boxes), end='\r')

            original_line = original_data['text'][i]
            character_count.append(len(original_line))

            #print("Matches for line: " + original_line + " at " + str(original_data['coord'][i]))
            ocr_line = easy_get_text_within(
                original_data['coord'][i], ocr_data)

            ratio = compare_line(original_line.rstrip(), ocr_line.rstrip())
            overall_ratios.append(ratio)

            if to_file:

                file_text += "CO> " + str(original_data['coord'][i])
                file_text += "\nOG> " + original_line.rstrip()
                file_text += "\nOC> "
                file_text += ocr_line.rstrip() + \
                    "\nRA> " + str(ratio) + "%\n"

    total_characters = sum(character_count)

    for i in range(n_boxes):
        overall_ratios[i] *= (character_count[i]/total_characters)

    if to_file:

        file_text = "OVERALL PRECISION: " + \
            str(round(sum(overall_ratios), 5)) + "%\n" + file_text
        with open(os.path.abspath('compare.txt'), "w+", encoding="utf-8") as fp:
            fp.write(file_text)

    return sum(overall_ratios)

# Retesteo

# ABBYY


def retest(file: str, to_file: str) -> dict:

    with open(file, "r+", encoding="utf-8") as fp:
        original = fp.readlines()

    previous_score = ""
    coord = []
    og_text = []
    ocr_text = []

    for line in original:

        if line[:4] == "CO> ":  # Coordenadas
            coord.append(line[4:])
        elif line[:4] == "OG> ":  # Texto Original
            og_text.append(line[4:])
        elif line[:4] == "OC> ":  # Texto OCR
            ocr_text.append(line[4:])
        elif line[:19] == "OVERALL PRECISION: ":
            previous_score = line[19:]

    n_boxes: int = len(og_text)
    overall_ratios: list = []
    character_count = []
    file_text: str = ""

    for i in range(n_boxes):

        print(str(i) + "/" + str(n_boxes), end='\r')

        original_line = og_text[i]
        ocr_line = ocr_text[i]
        line_coord = coord[i]
        character_count.append(len(original_line))
        ratio = 0.0

        if ocr_text == "NO MATCH":
            overall_ratios.append(
                0.0)  # Si no encuentra un match, agrega un ratio = 0.0%
        else:
            ratio = compare_line(original_line.rstrip(), ocr_line.rstrip())
            overall_ratios.append(ratio)

        file_text += "CO> " + str(line_coord)
        file_text += "\nOG> " + original_line.rstrip()
        file_text += "\nOC> "
        if ocr_text != "NO MATCH":
            file_text += ocr_line.rstrip() + \
                "\nRA> " + str(ratio) + "%\n"
        else:
            file_text += "NO MATCH" + "\nRA> 0.0%\n"

    total_characters = sum(character_count)

    for i in range(n_boxes):
        overall_ratios[i] *= (character_count[i]/total_characters)

    file_text = "OVERALL PRECISION: " + \
        str(round(sum(overall_ratios), 5)) + "%\n" + file_text
    with open(os.path.abspath(to_file + '.txt'), "w+", encoding="utf-8") as fp:
        fp.write(file_text)

    return previous_score + " -> " + str(sum(overall_ratios))
