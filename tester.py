"""
    testers.py

    Hace OCR a una imagen y hace pruebas de eficacia.

    Uso:
    tester.py [-h] [-i ITERACIONES] (-t | -a | -g | -e | -eg | -az CLAVE_AZURE) imagen_entrada salida

    Argumentos:
    imagen_entrada        ubicación de la imagen
    salida                nombre del archivo de salida
    -i, --iteraciones número de iteraciones (default 50)
    -t, --tesseract utiliza tesseract como motor
    -a, --abbyy     utiliza el archivo de OCR de ABBYY para comparar
    -g, --google    utiliza Google Cloud Vision como motor (NOTA: setear la API Key como variable de entorno)
    -e, --easyocr   utiliza EasyOCR como motor (con CPU)
    -eg, --easyocrgpu   utiliza EasyOCR como motor (con GPU)
    -az, --azure    utiliza Azure como motor, pasar clave de suscripción como argumento

    Opcionales:
    -h, --help      mensaje de ayuda

"""

import os
import argparse
from statistics import mean

import ocr_parser as Parser
import ocr_benchmark as Benchmark

import time

# Parseo de argumentos
parser = argparse.ArgumentParser(
    description='Hace OCR a una imagen y lo compara con el texto original por coordenadas. Devuelve por consola la precisión del OCR. Opcionalmente devuelve un txt con la comparación entre los textos.')

parser.add_argument('imagen_entrada', type=str, help='ubicación de la imagen')
parser.add_argument('salida', type=str, help='nombre del archivo de salida')
parser.add_argument('-i', '--iteraciones', type=int, default=50,
                    help='número de iteraciones')
group = parser.add_mutually_exclusive_group(
    required=True)
group.add_argument('-t', '--tesseract', dest='motor_tesseract',
                   action='store_true', help='utiliza tesseract como motor')
group.add_argument('-a', '--abbyy', dest='motor_abbyy',
                   action='store_true', help='utiliza el archivo de entrada de ABBYY')
group.add_argument('-g', '--google', dest='motor_google',
                   action='store_true', help='utiliza el archivo de entrada de Google')
group.add_argument('-e', '--easyocr', dest='motor_easyocr',
                   action='store_true', help='utiliza EasyOCR como motor (con CPU)')
group.add_argument('-eg', '--easyocrgpu', dest='motor_easyocrgpu',
                   action='store_true', help='utiliza EasyOCR como motor (con GPU)')
group.add_argument('-az', '--azure', dest='clave_azure',
                   type=str, help='utiliza Azure como motor, pasar clave de suscripción como argumento')

args = parser.parse_args()

# File handling

if not args.clave_azure and not os.path.isfile(args.imagen_entrada):
    raise FileNotFoundError(
        f"[Errno 2] No such file or directory: '{args.imagen_entrada}'")

# OCR

image_file = args.imagen_entrada

test_data = {'time': [], 'cpu': [], 'ram': []}

benchmark = Benchmark.Benchmark()

file_text = "Test number: Tiempo, CPU, RAM\n"

i = 1

while(i <= args.iteraciones):

    print(str(i) + "/" + str(args.iteraciones))

    if(args.motor_tesseract):

        benchmark.run()
        t_data = Parser.get_tesseract(image_file)
        benchmark.stop()

        print([benchmark.delta_time, benchmark.cpu_usage, benchmark.ram_usage])
        test_data['time'].append(benchmark.delta_time)
        test_data['cpu'].append(benchmark.cpu_usage)
        test_data['ram'].append(benchmark.ram_usage)
        file_text += "Test " + \
            str(i) + ": " + str([benchmark.delta_time,
                                 benchmark.cpu_usage, benchmark.ram_usage]) + "\n"

    elif(args.motor_abbyy):

        pass

    elif(args.motor_google):

        benchmark.run()
        ocr_data = Parser.google_to_document(image_file)
        benchmark.stop()

        print([benchmark.delta_time, benchmark.cpu_usage, benchmark.ram_usage])
        test_data['time'].append(benchmark.delta_time)
        test_data['cpu'].append(benchmark.cpu_usage)
        test_data['ram'].append(benchmark.ram_usage)
        file_text += "Test " + \
            str(i) + ": " + str([benchmark.delta_time,
                                 benchmark.cpu_usage, benchmark.ram_usage]) + "\n"

    elif(args.motor_easyocr or args.motor_easyocrgpu):

        benchmark.run()
        e_data = Parser.get_easyocr(image_file, args.motor_easyocrgpu)
        benchmark.stop()

        print([benchmark.delta_time, benchmark.cpu_usage, benchmark.ram_usage])
        test_data['time'].append(benchmark.delta_time)
        test_data['cpu'].append(benchmark.cpu_usage)
        test_data['ram'].append(benchmark.ram_usage)
        file_text += "Test " + \
            str(i) + ": " + str([benchmark.delta_time,
                                 benchmark.cpu_usage, benchmark.ram_usage]) + "\n"

    elif(args.clave_azure):

        try:
            benchmark.run()
            Parser.get_azure(image_file, args.clave_azure)
        except Exception as e:
            benchmark.stop()
            print(e)
            i -= 1
        else:
            benchmark.stop()

            print([benchmark.delta_time, benchmark.cpu_usage, benchmark.ram_usage])
            test_data['time'].append(benchmark.delta_time)
            test_data['cpu'].append(benchmark.cpu_usage)
            test_data['ram'].append(benchmark.ram_usage)
            file_text += "Test " + \
                str(i) + ": " + str([benchmark.delta_time,
                                     benchmark.cpu_usage, benchmark.ram_usage]) + "\n"

    benchmark.reset()
    i += 1

print("Final results: ")
print([mean(test_data['time']), mean(test_data['cpu']), mean(test_data['ram'])])

file_text += "Final results: " + \
    str([mean(test_data['time']), mean(test_data['cpu']), mean(test_data['ram'])])
with open(os.path.abspath(args.salida + '.txt'), "w+", encoding="utf-8") as fp:
    fp.write(file_text)
