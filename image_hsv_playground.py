"""
    image_hsv_playground.py

    Permite editar los parámetros de color en sistema HSV de una imagen para realizar un filtro por colores.
    Indicar el path absoluto de la imagen o poner el archivo '.py' y la imagen en la misma carpeta.

    Devuelve por consola los parámetros seleccionados (por si fueran necesarios para algún filtro).

    Uso:
    image_hsv_playground.py [-h] [-m] imagen_entrada

    Argumentos:
    imagen_entrada  ubicación de la imagen.

    Opcionales:
    -m, --mask      muestra la mascara
    -h, --help      mensaje de ayuda
"""

import argparse, sys, os
import cv2
import numpy as np

# DEFINES: ------------------------------------------------------------------
ESC = 27


######## Parseo de argumentos

parser = argparse.ArgumentParser(description='--> Permite editar los parámetros de color en sistema HSV de una imagen para realizar un filtro por colores.')
parser.add_argument('imagen_entrada', type=str, help='ubicación de la imagen.')
parser.add_argument('-m','--mask', dest='show_mask', action='store_true', default=False, help='show the mask image')
args = parser.parse_args()

show_mask = args.show_mask

######## File handling

if not os.path.isfile(args.imagen_entrada): 
    raise FileNotFoundError(f"[Errno 2] No such file or directory: '{args.imagen_entrada}'")


# PROGRAM: ------------------------------------------------------------------

# Get image name.
image_name = args.imagen_entrada

# Load image as BGR.
imagen_bgr = cv2.imread(image_name, cv2.IMREAD_COLOR)
cv2.imshow("Noticia (BGR):", imagen_bgr)
cv2.waitKey(0)
cv2.destroyWindow("Noticia (BGR):")

# Transform image to HSV.
imagen_hsv = cv2.cvtColor(imagen_bgr,cv2.COLOR_BGR2HSV)
cv2.imshow("Noticia (HSV):", imagen_hsv)
cv2.waitKey(0)
cv2.destroyWindow("Noticia (HSV):")

# Genero valores HSV iniciales.
hsv_min_values = np.array( [   0,   0,   0] )
hsv_max_values = np.array( [ 360, 255, 255] )

# Genero las slide-bars para variar los valores de H, S y V.
cv2.namedWindow('Set Color Parameter')
cv2.createTrackbar('H_Min', 'Set Color Parameter', hsv_min_values[0], 360, lambda _:_)
cv2.createTrackbar('H_Max', 'Set Color Parameter', hsv_max_values[0], 360, lambda _:_)
cv2.createTrackbar('S_Min', 'Set Color Parameter', hsv_min_values[1], 255, lambda _:_)
cv2.createTrackbar('S_Max', 'Set Color Parameter', hsv_max_values[1], 255, lambda _:_)
cv2.createTrackbar('V_Min', 'Set Color Parameter', hsv_min_values[2], 255, lambda _:_)
cv2.createTrackbar('V_Max', 'Set Color Parameter', hsv_max_values[2], 255, lambda _:_)

cv2.setTrackbarPos('H_Min', 'Set Color Parameter', hsv_min_values[0])
cv2.setTrackbarPos('H_Max', 'Set Color Parameter', hsv_max_values[0])
cv2.setTrackbarPos('S_Min', 'Set Color Parameter', hsv_min_values[1])
cv2.setTrackbarPos('S_Max', 'Set Color Parameter', hsv_max_values[1])
cv2.setTrackbarPos('V_Min', 'Set Color Parameter', hsv_min_values[2])
cv2.setTrackbarPos('V_Max', 'Set Color Parameter', hsv_max_values[2])

# HSV playground.
while(True):
    # Actualizo el valor del color.
    hsv_min_values[0] = cv2.getTrackbarPos('H_Min', 'Set Color Parameter')
    hsv_max_values[0] = cv2.getTrackbarPos('H_Max', 'Set Color Parameter')
    hsv_min_values[1] = cv2.getTrackbarPos('S_Min', 'Set Color Parameter')
    hsv_max_values[1] = cv2.getTrackbarPos('S_Max', 'Set Color Parameter')
    hsv_min_values[2] = cv2.getTrackbarPos('V_Min', 'Set Color Parameter')
    hsv_max_values[2] = cv2.getTrackbarPos('V_Max', 'Set Color Parameter')

    # Threshold the HSV image to get only selected color.
    mask = cv2.inRange(imagen_hsv, hsv_min_values, hsv_max_values)

    # Muestro la imagen mascara.
    if show_mask: cv2.imshow('Mask', mask)

    # Bitwise-AND mask the original image
    mask_colored = cv2.bitwise_and(imagen_bgr, imagen_bgr, mask=mask)
    cv2.imshow('Colored Mask', mask_colored)

    # La condicion de salir es apretando ESC == 27.
    if (cv2.waitKey(1) & 0xFF) == ESC:
        cv2.destroyWindow('Set Color Parameter')
        break

# Print Values of mask.
print( 'Hue:        [' + str(hsv_min_values[0]) + ', ' + str(hsv_max_values[0]) + ']')
print( 'Saturation: [' + str(hsv_min_values[1]) + ', ' + str(hsv_max_values[1]) + ']')
print( 'Value:      [' + str(hsv_min_values[2]) + ', ' + str(hsv_max_values[2]) + ']')

# Wait for one last key press.
cv2.waitKey(0)

# When everything is done, release the capture.
cv2.destroyAllWindows()