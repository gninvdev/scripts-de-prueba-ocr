"""
    compare_full_text.py

    Hace OCR a una imagen y lo compara con el texto original.

    Devuelve por consola el tiempo que tarda el OCR y la precisión.
    Opcionalmente devuelve un html con la comparación entre los textos.

    Uso:
    compare_full_text.py [-h] [--html] (-t | -e) imagen_entrada texto_entrada

    Argumentos:
    imagen_entrada  ubicación de la imagen
    texto_entrada   ubicación del texto original
    -t, --tesseract utiliza tesseract como motor
    -e, --easyocr   utiliza EasyOCR como motor (con CPU)
    -eg, --easyocrgpu utiliza EasyOCR como motor (con GPU)

    Opcionales:
    -h, --help      mensaje de ayuda
    --html          crea un html con las diferencias
"""

import os
import datetime
import argparse

import ocr_parser as Parser
import full_text_comparator as Comparator

# Parseo de argumentos

parser = argparse.ArgumentParser(
    description='Hace OCR a una imagen y lo compara con el texto original. Devuelve por consola el tiempo que tarda el OCR y la precisión. Opcionalmente devuelve un html con la comparación entre los textos.')

parser.add_argument('imagen_entrada', type=str, help='ubicación de la imagen')
parser.add_argument('texto_entrada', type=str,
                    help='ubicación del texto original')
parser.add_argument('--html', dest='crear_html',
                    action='store_true', help='crea un html con las diferencias')
group = parser.add_mutually_exclusive_group(required=True)
group.add_argument('-t', '--tesseract', dest='motor_tesseract',
                   action='store_true', help='utiliza tesseract como motor')
group.add_argument('-e', '--easyocr', dest='motor_easyocr',
                   action='store_true', help='utiliza EasyOCR como motor (con CPU)')
group.add_argument('-eg', '--easyocrgpu', dest='motor_easyocrgpu',
                   action='store_true', help='utiliza EasyOCR como motor (con GPU)')

args = parser.parse_args()

# File handling

if not os.path.isfile(args.imagen_entrada):
    raise FileNotFoundError(
        f"[Errno 2] No such file or directory: '{args.imagen_entrada}'")

with open(args.texto_entrada, "r+", encoding="utf-8") as fp:
    texto_original = fp.read()

# OCR

if args.motor_tesseract:
    tiempo_inicial = datetime.datetime.now()
    texto_ocr = Parser.full_text_tesseract(args.imagen_entrada)
    tiempo_final = datetime.datetime.now()
elif args.motor_easyocr or args.motor_easyocrgpu:
    tiempo_inicial = datetime.datetime.now()
    texto_ocr = Parser.full_text_easyocr(
        args.imagen_entrada, args.motor_easyocrgpu)
    tiempo_final = datetime.datetime.now()

print("Velocidad: " + str(tiempo_final-tiempo_inicial))

print("Precisión: " +
      str(Comparator.full_text_compare(texto_original, texto_ocr, args.crear_html)) + "%")
