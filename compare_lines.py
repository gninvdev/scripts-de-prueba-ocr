"""
    compare_lines.py

    Hace OCR a una imagen y lo compara con el texto original.
    A diferencia de comparacion_ocr.txt, este compara por coordenadas.

    Devuelve por consola la precisión del OCR.
    Opcionalmente devuelve un txt con la comparación entre los textos.

    Uso:
    compare_lines.py [-h] [--txt] (-t | -a | -g | -e | -eg | -az clave_azure) imagen_entrada ocr_entrada texto_entrada

    Argumentos:
    imagen_entrada  ubicación de la imagen
    ocr_entrada   ubicación del texto de ABBYY con coordenadas
        (no acepta el .OCR directo, hay que pasarlo a otra codificación antes)
    texto_entrada   ubicación del texto original
    -t, --tesseract utiliza tesseract como motor
    -a, --abbyy     utiliza el archivo de OCR de ABBYY para comparar
    -g, --google    utiliza Google Cloud Vision como motor (NOTA: setear la API Key como variable de entorno)
    -e, --easyocr   utiliza EasyOCR como motor (con CPU)
    -eg, --easyocrgpu   utiliza EasyOCR como motor (con GPU)
    -az, --azure    utiliza Azure como motor, pasar clave de suscripción como argumento

    Opcionales:
    -h, --help      mensaje de ayuda
    --txt           crea un txt con las diferencias
        Formato:
            OVERALL PRECISION: Precisión promedio
            CO>: Coordenadas (Original -> Match)
            OG>: Texto del original
            OC>: Texto del OCR
            RA>: Ratio de precisión

"""

import os
import argparse

import ocr_parser as Parser
import full_text_comparator as Comparator

# Parseo de argumentos
parser = argparse.ArgumentParser(
    description='Hace OCR a una imagen y lo compara con el texto original por coordenadas. Devuelve por consola la precisión del OCR. Opcionalmente devuelve un txt con la comparación entre los textos.')

parser.add_argument('imagen_entrada', type=str, help='ubicación de la imagen')
parser.add_argument('ocr_entrada', type=str,
                    help='ubicación de las coordenadas de OCR (no acepta el .OCR directo)')
parser.add_argument('texto_entrada', type=str,
                    help='ubicación del texto original')
parser.add_argument('--txt', dest='crear_txt',
                    action='store_true', help='crea un txt con las diferencias')
group = parser.add_mutually_exclusive_group(
    required=True)
group.add_argument('-t', '--tesseract', dest='motor_tesseract',
                   action='store_true', help='utiliza tesseract como motor')
group.add_argument('-a', '--abbyy', dest='motor_abbyy',
                   action='store_true', help='utiliza el archivo de entrada de ABBYY')
group.add_argument('-g', '--google', dest='motor_google',
                   action='store_true', help='utiliza el archivo de entrada de Google')
group.add_argument('-e', '--easyocr', dest='motor_easyocr',
                   action='store_true', help='utiliza EasyOCR como motor (con CPU)')
group.add_argument('-eg', '--easyocrgpu', dest='motor_easyocrgpu',
                   action='store_true', help='utiliza EasyOCR como motor (con GPU)')
group.add_argument('-az', '--azure', dest='clave_azure',
                   type=str, help='utiliza Azure como motor, pasar clave de suscripción como argumento')

args = parser.parse_args()

# File handling

if not args.clave_azure and not os.path.isfile(args.imagen_entrada):
    raise FileNotFoundError(
        f"[Errno 2] No such file or directory: '{args.imagen_entrada}'")

if not os.path.isfile(args.texto_entrada):
    raise FileNotFoundError(
        f"[Errno 2] No such file or directory: '{args.texto_entrada}'")

if not os.path.isfile(args.ocr_entrada):
    raise FileNotFoundError(
        f"[Errno 2] No such file or directory: '{args.ocr_entrada}'")

# OCR

image_file = args.imagen_entrada
original_file = args.ocr_entrada
original_text_file = args.texto_entrada

original_data = Parser.corrected_file_to_line_data(
    original_file, original_text_file)

ocr_data: dict

if(args.motor_tesseract):
    t_data = Parser.get_tesseract(image_file)
    ocr_data = Parser.tesseract_to_line_data(t_data)
    print(Comparator.compare_line_data(
        original_data, ocr_data, "tesseract", args.crear_txt))
elif(args.motor_abbyy):
    ocr_data = Parser.abbyy_file_to_line_data(original_file)
    print(Comparator.compare_line_data(
        original_data, ocr_data, "abbyy", args.crear_txt))
elif(args.motor_google):
    ocr_data = Parser.google_to_document(image_file)
    print(Comparator.compare_line_data(
        original_data, ocr_data, "google", args.crear_txt))
elif(args.motor_easyocr or args.motor_easyocrgpu):
    e_data = Parser.get_easyocr(image_file, args.motor_easyocrgpu)
    ocr_data = Parser.easyocr_to_line_data(e_data)
    print(Comparator.compare_line_data(
        original_data, ocr_data, "easyocr", args.crear_txt))
elif(args.clave_azure):
    a_data = Parser.get_azure(image_file, args.clave_azure)
    ocr_data = Parser.azure_to_line_data(a_data)
    print(Comparator.compare_line_data(
        original_data, ocr_data, "azure", args.crear_txt))

