from corrector import Corrector
import difflib
from os import listdir

corrector = Corrector()

def line_comparator(line1: str, line2: str) -> float:
    m = difflib.SequenceMatcher(None, line1, line2)

    return round(m.ratio() * 100, 5)

def text_comparator(lines1 : list, lines2 : list) -> float:
    total_chars = sum([len(i) for i in lines1])

    res = 0
    for i in range(len(lines1)):
        res += line_comparator(lines1[i], lines2[i]) * len(lines1[i]) / total_chars

    return res


def get_first_word(line : str):
    res = ''
    for char in line:
        if char != ' ':
            res += char
        else:
            break

    return res

def line_getter_no_dash(path: str, ocr_line = False) -> list:
    module = 4

    if ocr_line:
        module = 0

    original_text = []
    compare_txt = open(path)

    lines = compare_txt.readlines()
    current_line = 1
    for line in lines:
        if current_line % 5 == module:
            original_text.append(line[4:].rstrip())

        current_line += 1
    compare_txt.close()


    for i in range(len(original_text)):
        for i in range(len(original_text)):
            if original_text[i] == '':
                continue
            if original_text[i][-1] == '-':
                post_dash = get_first_word(original_text[i + 1])

                original_text[i] = original_text[i][:-1] + post_dash
                original_text[i + 1] = original_text[i + 1][len(post_dash) + 1:]

    return original_text


def get_no_corrected_percentage(path):
    fline = open(path).readline().rstrip().split()[-1]
    return float(fline[:-1])


def compare_corrected_text(path : str, save=False): #devuelve la comparacion con postproceso y sin. Save = true guarda un txt mostrando el postproceso
    original_lines = line_getter_no_dash(path)
    ocr_lines = line_getter_no_dash(path, ocr_line=True)
    corrected_lines = [corrector.correct_text(line) for line in line_getter_no_dash(path, ocr_line=True)]

    result_no_corrected = text_comparator(ocr_lines, original_lines)
    result_corrected =  text_comparator(corrected_lines, original_lines)

    if save:
        corrected_path = f"{path.split('.')[0]}_corregido.txt"
        corrected_path = f'corregidos/{"/".join(corrected_path.strip("/").split("/")[1:])}'

        f = open(corrected_path, 'w')
        f.write(f'{str(result_no_corrected)} {str(result_corrected)}\n')
        f.write('\n')

        for i in range(len(corrected_lines)):
            f.write(original_lines[i] + '\n')
            f.write(ocr_lines[i] + '\n')
            f.write(corrected_lines[i] + '\n')
            f.write(f'{str(line_comparator(ocr_lines[i], original_lines[i]))} {str(line_comparator(corrected_lines[i], original_lines[i]))}\n')
            f.write('\n')

        f.close()
    return (result_no_corrected, result_corrected)


def get_files(path : str) -> list:
    return [file for file in listdir(path)]


def compare_ocr_engines(path : str) -> dict: #devuelve un diccionario con los resultados del postproceso para cada archivo por cada motor
    ocr_engines = get_files(path)

    results = {}

    for engine in ocr_engines:
        results[engine] = []
        for compare_file in get_files(path + '/' + engine):
            results[engine].append(compare_corrected_text(path + '/' + engine + '/' + compare_file, save=True))
            print(path + '/' + engine + '/' + compare_file)
    return results

def get_all_correctors(path : str): #devuelve diccionario a cada corrector, cada corrector tiene un  diccionario a cada motor y cada motor tiene los resultados de cada archivo
    res = {"old_corrector" : None , "google_corrector" : None, "corrector" : None}

    res["corrector"] = compare_ocr_engines(path)

    corrector.corrector_type = 1

    res["google_corrector"] = compare_ocr_engines(path)

    corrector.corrector_type = 0

    res["old_corrector"] = compare_ocr_engines(path)

    return res