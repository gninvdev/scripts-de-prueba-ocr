import enchant
import re
from math import log
from spellchecker import SpellChecker
import requests
import SymSpellCppPy
from bs4 import BeautifulSoup


class Corrector:
    def __init__(self):
        self.enchant_corrector = enchant.Dict('es_ES')
        self.speller = SpellChecker(language='es')
        self.openings = '([{"'

        self.symspell = SymSpellCppPy.SymSpell()
        self.symspell.load_dictionary(corpus="es.txt", term_index=0, count_index=1, separator=" ")

        self.corrector_type = None  # corrector == 0 old_corrector, corrector == 1 google_corrector, todos los demas el nuevo corrector

    def split_text(self, text: str) -> list:
        return re.findall(r'[\w-]+|\S', text)

    def correct_text(self, text: str) -> str:
        correct = self.split_text(text)

        res = ''
        stop_space = False

        for word in correct:
            if len(word) == 1 and not word.isalnum():
                if word in self.openings:
                    res += ' ' + word
                    stop_space = True
                else:
                    res += word

            elif stop_space:
                if self.corrector_type == 0:
                    res += self.old_word_corrector(word)
                elif self.corrector_type == 1:
                    res += self.google_word_corrector(word)
                else:
                    res += self.word_corrector(word)

                stop_space = False

            else:
                if self.corrector_type == 0:
                    res += ' ' + self.old_word_corrector(word)
                elif self.corrector_type == 1:
                    res += ' ' + self.google_word_corrector(word)
                else:
                    res += ' ' + self.word_corrector(word)

        return res.strip()

    def word_corrector(self, word: str) -> str:
        if (self.speller.unknown([word])) and not word[0].isupper():
            return self.symspell.word_segmentation(word).corrected_string

        return word

    def old_word_corrector(self, word: str) -> str:
        if (self.speller.unknown([word])) and not word[0].isupper():
            if self.speller.correction(word) != word and (
                    len(word) - 1 <= len(self.speller.correction(word)) <= len(word) + 1):
                return self.speller.correction(word)
            else:
                return self.infer_spaces(word)

        return word

    def infer_spaces(self, s: str) -> str:
        """Uses dynamic programming to infer the location of spaces in a string
        without spaces.

        FROM: https://stackoverflow.com/questions/8870261/how-to-split-text-without-spaces-into-list-of-words/11642687#11642687
        """

        # Find the best match for the i first characters, assuming cost has
        # been built for the i-1 first characters.
        # Returns a pair (match_cost, match_length).
        words = open("dicc.txt").read().split()
        wordcost = dict((k, log((i + 1) * log(len(words)))) for i, k in enumerate(words))
        maxword = max(len(x) for x in words)

        def best_match(i):
            candidates = enumerate(reversed(cost[max(0, i - maxword):i]))
            return min((c + wordcost.get(s[i - k - 1:i], 9e999), k + 1) for k, c in candidates)

        # Build the cost array.
        cost = [0]
        for i in range(1, len(s) + 1):
            c, k = best_match(i)
            cost.append(c)

        # Backtrack to recover the minimal-cost string.
        out = []
        i = len(s)
        while i > 0:
            c, k = best_match(i)
            assert c == cost[i]
            out.append(s[i - k:i])
            i -= k

        return " ".join(reversed(out))

    def google_word_corrector(self, word: str) -> str:
        if (self.speller.unknown([word])) and not word[0].isupper():
            url = 'https://www.google.com/search?q=' + word

            headers = {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', }

            page = requests.get(url, headers=headers)
            soup = BeautifulSoup(page.content, 'html.parser')

            if (res := soup.select('div i')):
                return res[0].text

        return word