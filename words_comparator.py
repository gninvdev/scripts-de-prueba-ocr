#################################################################################################
############################## CLASES TEXT Y TEXTS_COMPARATOR ###################################
#################################################################################################

# Example --------------------------------------------

# text_1 = "Hola como como estas"
# text_2 = "Hola como estoy muy"

# Cmp = TextsComparator(text_1, text_2)
# Cmp.printInfo()
# print(Cmp.percentage_ratios())



# TEXT Class -----------------------------------------

# Contiene un texto estructurado de todas sus formas.
class Text:
    as_string = ''
    as_word_list = []
    as_words_count_dict = {}
    
    
    def __init__(self, str_1):
        self.as_string = str_1
        self.as_word_list = str_1.split(' ')
        self.as_words_count_dict = self.createUniqueWordsCountDict(self.as_word_list)
        
        
    # Crea un diccionario con las palabras del texto y la cuenta. {'palabra': #apariciones, ...}
    def createUniqueWordsCountDict(self, list_of_words):
        unique_words_dict = {}
        for word in list_of_words:
            if word in unique_words_dict:
                unique_words_dict[word] += 1
            else:
                unique_words_dict[word] = 1
        return unique_words_dict
    
    
    def printInfo(self):
        print(f"\tString[{len(self.as_string)}]: {self.as_string[0:20]} ...")
        print(f"\tList[{len(self.as_word_list)}]: {self.as_word_list[0:5]}")
        print(f"\tDict[{len(self.as_words_count_dict)}]: {dict(list(self.as_words_count_dict.items())[0:5])}")



# TEXTS_COMPARATOR Class ---------------------------------

# Contiene dos textos y todas sus operaciones.
class TextsComparator:
    
    text_1 = Text('')
    text_2 = Text('')
    
    
    def __init__(self, str_1, str_2):
        self.text_1 = Text(str_1)
        self.text_2 = Text(str_2)
        
        
    def printInfo(self):
        print("TEXT 1 ---------")
        self.text_1.printInfo()
        print('')
        print("TEXT 2 ---------")
        self.text_2.printInfo()


    # Devuelve un diccionario con las palabras que estan en un texto y no en el otro.
    def getRepeatedAndUniqueWordsInTextOne(self, first_text: Text, second_text: Text):
        all_words = set(first_text.as_word_list + second_text.as_word_list)
        words_in_1_but_not_in_2 = {}    # Missmatches_1
        words_in_2_but_not_in_1 = {}    # Missmatches_2
        words_in_1_and_in_2 = {}        # Matches
        for word in all_words:
            # Matches:
            if (word in first_text.as_word_list) and (word in second_text.as_word_list):
                word_diff = first_text.as_words_count_dict[word] - second_text.as_words_count_dict[word]
                word_min_appearences = min(first_text.as_words_count_dict[word],second_text.as_words_count_dict[word])
                print(f"Word: {word} has {word_diff} and {word_min_appearences}")
                if word_diff > 0: # More times in text_1 than in text_2.
                    words_in_1_but_not_in_2[word] = word_diff
                    words_in_1_and_in_2[word] = word_min_appearences
                elif word_diff < 0: # More times in text_2 than in text_1.
                    words_in_2_but_not_in_1[word] = word_diff
                    words_in_1_and_in_2[word] = word_min_appearences
                else: # word_diff = 0 --> Equal times in text_1 and in text_2
                    words_in_1_and_in_2[word] = word_min_appearences
            # Missmatches_1:
            elif (word in first_text.as_word_list) and (word not in second_text.as_word_list):
                words_in_1_but_not_in_2[word] = first_text.as_words_count_dict[word]
            # Missmatches_2:
            elif (word not in first_text.as_word_list) and (word in second_text.as_word_list):
                words_in_2_but_not_in_1[word] = second_text.as_words_count_dict[word]
            # Error case:
            else:
                print(f"Something went wrong with word: {word} !!!")

        return words_in_1_and_in_2, words_in_1_but_not_in_2, words_in_2_but_not_in_1
    
    
    # Devuelve un diccionario con TODAS las palabras que no coinciden.
    def getAllMatchesAndMismatchesWords(self):
        matches, missmatches_in_1, missmatches_in_2 = self.getRepeatedAndUniqueWordsInTextOne(self.text_1, self.text_2)
        all_missmatches = {}
        for some_dict in [missmatches_in_1, missmatches_in_2]: all_missmatches.update(some_dict)
        return matches, all_missmatches
    
    
    # Devuelve el ratio de palabras iguales entre los textos con las fórmulas:
    # 1. libDiff ratio: ( 2 * #MATCHES ) / ( #TOTAL_WORDS )                --> [0, 1]
    # 2. other ratio:   ( #TOTAL_WORDS - #MISMATCHES ) / ( #TOTAL_WORDS )  --> [0, 1]
    def words_ratios(self):
        total_words = len(self.text_1.as_word_list) + len(self.text_2.as_word_list)
        matches_dict, missmatches_dict = self.getAllMatchesAndMismatchesWords()
        total_matches = sum(matches_dict.values())
        total_missmatches = sum(missmatches_dict.values())
        libDiff_ratio = ( 2 * total_matches ) / total_words
        other_ratio = ( total_words - total_missmatches ) / total_words
        return libDiff_ratio, other_ratio


    # Devuelve el porcentaje de igualdad de los textos de 0% a 100%.
    def percentage_ratios(self, decimals=3):
        libDiff_ratio, other_ratio = self.words_ratios()
        return round(100*libDiff_ratio, decimals), round(100*other_ratio, decimals)