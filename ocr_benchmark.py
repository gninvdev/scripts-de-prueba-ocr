import psutil
import threading
import time


class Benchmark:
    def __init__(self):
        self.cpu_usage = 0  # uso del cpu
        self.ram_usage = 0  # uso de la ram

        self.cpu_data = []  # estas dos son para poder hacer graficos
        self.ram_data = []

        self.running = False

    def run(self):
        self.running = True
        self.thread = threading.Thread(target=self.benchmark)
        self.thread.start()

    def stop(self):
        self.running = False
        self.thread.join()

    def reset(self):
        self.cpu_usage = 0
        self.ram_usage = 0

        self.cpu_data = []
        self.ram_data = []

        self.running = False

        self.start_time = None
        self.delta_time = None
        self.final_time = None

    def benchmark(self):
        times_to_check = 10

        sum_cpu_usage = 0
        sum_ram_usage = 0

        times_run = 0

        self.start_time = time.time()

        while self.running:
            cpu_inner_sum = 0
            ram_inner_sum = 0

            for i in range(times_to_check):
                cpu = psutil.cpu_percent(interval=0.1)
                ram = psutil.virtual_memory().percent

                cpu_inner_sum += cpu
                ram_inner_sum += ram

                # tiempo que tarda en fijarse entre cada iteracion
                time.sleep(0.1)

            sum_cpu_usage += cpu_inner_sum / times_to_check
            sum_ram_usage += ram_inner_sum / times_to_check

            times_run += 1

            self.cpu_data.append(cpu_inner_sum / times_to_check)
            self.ram_data.append(ram_inner_sum / times_to_check)

            self.cpu_usage = sum_cpu_usage / times_run
            self.ram_usage = sum_ram_usage / times_run

        self.final_time = time.time()

        self.delta_time = self.final_time - self.start_time


"""  
EJEMPLO

b = Benchmark()
b.run()

(codigo a testear)

b.stop()

llamo a lo que necesito, cpu_usage,ram_usage,delta_time

b.reset() resetea los valores
"""
