"""
    recompare.py

    Recompara un archivo de salida de compare_lines.py

    Uso:
    recompare.py [-h] texto_entrada nombre_del_archivo

    Argumentos:
    texto_entrada   ubicación del texto de comparación
    nombre_del_archivo nombre del archivo de salida
        Formato:
                OVERALL PRECISION: Precisión promedio
                CO>: Coordenadas (Original -> Match)
                OG>: Texto del original
                OC>: Texto del OCR
                RA>: Ratio de precisión

    Opcionales:
    -h, --help      mensaje de ayuda

"""

import os
import argparse

import full_text_comparator as Comparator

# Parseo de argumentos
parser = argparse.ArgumentParser(
    description='Recompara un archivo de salida de compare_lines.py')

parser.add_argument('texto_entrada', type=str,
                    help='ubicación del texto de comparación')
parser.add_argument('nombre_del_archivo', type=str,
                    default="compare", help='crea un txt con las diferencias')

args = parser.parse_args()

# File handling

if not os.path.isfile(args.texto_entrada):
    raise FileNotFoundError(
        f"[Errno 2] No such file or directory: '{args.texto_entrada}'")

# OCR

text_file = args.texto_entrada

print(Comparator.retest(text_file, args.nombre_del_archivo))
